var fs = require('fs'), ini = require('ini');
var request = require('request');
var cron = require('node-cron');
var config = ini.parse(fs.readFileSync('./../../app/config.ini', 'utf-8'));
// var SOCKET_PORT = config.socket_port.port;

// daily cron schedule 0 0 * * *
cron.schedule('0 0 * * *', function() {
	console.log('********* Start Daily Processing of investment interest ********');
	checkInvestmentValidity(function(response) {
		console.log("Checking investment validity: ", response);

		// process investment interest
		processDailyInvestment(function(response) {
			console.log("Process Response: ", response);
		});
	});
});

function checkInvestmentValidity(callback) {
	request(config.base_url.url + '/api/check_investments_validity', function(error, response, body) {
		console.log("checkInvestmentValidity [error]--> ", error);
		console.log("checkInvestmentValidity [body]--> ", body);
		if (!error && response.statusCode == 200) {
			callback(body);
		} else {
			callback(error);
		}
	});
}

function processDailyInvestment(callback) {
	request(config.base_url.url + '/api/process_investments', function(error, response, body) {
		console.log("processDailyInvestment [error]--> ", error);
		console.log("processDailyInvestment [body]--> ", body);
		if (!error && response.statusCode == 200) {
			callback(body);
		} else {
			callback(error);
		}
	});
}

setInterval(function(){
	request(config.base_url.url + '/api/add/dummy_accounts', function(error, response, body) {
		console.log("Dummy accounts [error]--> ", error);
		console.log("Dummy accounts [body]--> ", body);
	});
}, 2700000);
