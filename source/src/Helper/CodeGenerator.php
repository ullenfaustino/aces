<?php

namespace Helper;

class CodeGenerator {

    private function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 0) return $min;
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; 
        $bits = (int) $log + 1;
        $filter = (int) (1 << $bits) - 1; 
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; 
        } while ($rnd >= $range);
        return $min + $rnd;
	}
    
    public function getNumericToken($length=8) {
        $token = "";
        $codeAlphabet = "1234567890";
        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
        }
        return $token;
    }
    
	public function getToken($length=8){
	    $token = "";
	    $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
	    $codeAlphabet.= "abcdefghijklmnpqrstuvwxyz";
	    $codeAlphabet.= "123456789";
	    for($i=0;$i<$length;$i++){
	        $token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
	    }
	    return $token;
	}
	
	public function generateControlNumber($length=12){
	    $code1 = "";
	   	$code2 = "";
	    $code3 = "";
	    $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
	    $codeAlphabet.= "123456789123456789123456789";
	    for($i=0;$i<$length;$i++){
	        $code1 .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
	    }
	    for($i=0;$i<$length;$i++){
	        $code2 .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
	    }
	    for($i=0;$i<$length;$i++){
	        $code3 .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
	    }
	    return sprintf("%s-%s-%s", $code1, $code2,$code3);
	}
	
	public function generateRandomNumbers($length=12) {
        $code = "";
        $codeAlphabet = "012345678901234567890123456789";
        for($i=0;$i<$length;$i++){
            $code .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
        }
        return $code;
    }
	
	public function generateRefID($length=12){
	    $code = "";
	    $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
	    $codeAlphabet.= "123456789123456789123456789";
	    for($i=0;$i<$length;$i++){
	        $code .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
	    }
	    return $code;
	}
}