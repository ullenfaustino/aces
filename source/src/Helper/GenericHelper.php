<?php

namespace Helper;

use \Illuminate\Database\Capsule\Manager as DB;
use \Request;
use \Users;
use \User;
use \Blocktrail\SDK\BlocktrailSDK;
use \GeneralSettings;
use \CompanyAllocatedIncome;
use \PurchaseLogs;
use \InvestmentLogs;
use \InterestLogs;
use \Payouts;
use \ConvertionLogs;
use \DirectReferral;
use \Messages;
use \MessageReplies;

class GenericHelper {

    public function initBitcoinClient() {
        return new BlocktrailSDK(BLOCKTRAIL_API_KEY, BLOCKTRAIL_API_SECRET, "BTC", BLOCKTRAIL_IS_TEST_BTC);
    }

    public function getClient() {
        return GenericHelper::initBitcoinClient();
    }

    public function sendPaymentBTC($username, $password, $amount, $sentToAddress = null) {
        if (is_null($sentToAddress)) {
            $sentToAddress = BLOCKTRAIL_COMPANY_WALLET;
        }

        $arrPayment = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $wallet = $client -> initWallet($username, $password);
            $value = BlocktrailSDK::toSatoshi($amount);
            $status = $wallet -> pay(array($sentToAddress => $value), false, true, \Blocktrail\SDK\Wallet::BASE_FEE);
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            throw new \Exception($e -> getMessage());
        }
    }

    public function sendMultiPaymentBTC($username, $password, $addresses) {
        $arrPayment = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $wallet = $client -> initWallet($username, $password);
            $status = $wallet -> pay($addresses, false, true, \Blocktrail\SDK\Wallet::BASE_FEE);
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            throw new \Exception($e -> getMessage());
        }
    }

    public function createNewWallet($username, $password) {
        $Arrwallet = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            list($wallet, $primaryMnemonic, $backupMnemonic, $blocktrailPublicKeys) = $client -> createNewWallet($username, $password);
            $address = $wallet -> getNewAddress();

            $Arrwallet['status'] = "Success";
            $Arrwallet['address'] = $address;
            $Arrwallet['username'] = $username;
            $Arrwallet['password'] = $password;
            $Arrwallet['primary_mnemonic'] = $primaryMnemonic;
            $Arrwallet['backup_mnemonic'] = $backupMnemonic;
            $Arrwallet['blocktrail_public_keys'] = $blocktrailPublicKeys;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $Arrwallet['status'] = "Error";
            $Arrwallet['message'] = $e -> getMessage();
        }
        return $Arrwallet;
    }

    public function investmentDateRange($investment, $range) {
        $due_date = $investment -> created_at;
        try {
            $endDate = new \DateTime($investment -> created_at);
            $endDate -> modify(sprintf('+%s day', $range));
            $endDay = $endDate -> format('Y-m-d');
            $due_date = $endDay;
        } catch (\Exception $e) {
            echo $e -> getMessage();
        }
        return $due_date;
    }

    public function generateNewAddress($username, $password) {
        $Arrwallet = array();
        try {
            $client = GenericHelper::initBitcoinClient();
            $wallet = $client -> initWallet($username, $password);
            $address = $wallet -> getNewAddress();
            $Arrwallet['address'] = $address;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $Arrwallet['status'] = "Error";
            $Arrwallet['message'] = $e -> getMessage();
        }
        return $Arrwallet;
    }

    public function setWalletWebHook($wallet_address, $user, $abc = "") {
        $client = GenericHelper::initBitcoinClient();
        $url = sprintf("%s/webhookHandler/%s", _BASE_URL, $wallet_address);
        $client -> setupWebhook($url, $user -> username . $abc);
        $client -> subscribeAddressTransactions($user -> username . $abc, $wallet_address, 1);
    }

    public function getWalletBalance($username, $password) {
        $arrBalance = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $wallet = $client -> initWallet($username, $password);
            list($confirmedBalance, $unconfirmedBalance) = $wallet -> getBalance();

            $arrBalance['status'] = "Success";
            $arrBalance['confirmed_balance'] = BlocktrailSDK::toBTC($confirmedBalance);
            $arrBalance['unconfirmed_balance'] = BlocktrailSDK::toBTC($unconfirmedBalance);
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $arrBalance['status'] = "Error";
            $arrBalance['message'] = $e -> getMessage();
        }
        return $arrBalance;
    }

    public function getAddressInfo($address) {
        $arrAddress = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $client_address = $client -> address($address);

            $arrAddress['status'] = "Success";
            $arrAddress['address'] = $client_address;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $arrAddress['status'] = "Error";
            $arrAddress['message'] = $e -> getMessage();
        }
        return $arrAddress;
    }

    public function getWalletTx($username, $password) {
        $arrTX = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $wallet = $client -> initWallet($username, $password);
            $transactions = $wallet -> transactions();

            $arrTX['status'] = "Success";
            $arrTX['transactions'] = $transactions;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $arrTX['status'] = "Error";
            $arrTX['message'] = $e -> getMessage();
        }
        return $arrTX;
    }

    public function decodeSatoshi($satoshi_value) {
        return BlocktrailSDK::toBTC($satoshi_value);
    }

    public function encodeSatoshi($btc) {
        return BlocktrailSDK::toSatoshi($btc);
    }

    public function convertToAUD_BUY($coin) {
        $coin_value = GeneralSettings::where("module_name", "=", "buy_value") -> first();
        return (double)$coin * (double)$coin_value -> content;
    }

    public function convertToAUD_SELL($coin) {
        $coin_value = GeneralSettings::where("module_name", "=", "sell_value") -> first();
        return (double)$coin * (double)$coin_value -> content;
    }

    public function convertBTC_TC_BUY($btc_value) {
        $buy_value_btc = GeneralSettings::where("module_name", "=", "buy_value_btc") -> first();
        return (double)$btc_value / (double)$buy_value_btc -> content;
    }

    public function convertTC_BTC_SELL($tc_coin) {
        $sell_value_btc = GeneralSettings::where("module_name", "=", "sell_value_btc") -> first();
        return (double)$tc_coin * (double)$sell_value_btc -> content;
    }

    public function convertTC_BTC_BUY($tc_coin) {
        $buy_value_btc = GeneralSettings::where("module_name", "=", "buy_value_btc") -> first();
        return (double)$tc_coin * (double)$buy_value_btc -> content;
    }

    public function convertTC_AUD_SELL($tc_coin) {
        $sell_value = GeneralSettings::where("module_name", "=", "sell_value") -> first();
        return (double)$tc_coin * (double)$sell_value -> content;
    }

    public function availableCoinBalance($user_id) {
        $sum_amount = PurchaseLogs::where("user_id", "=", $user_id) -> sum('tc_amount');
        $invested_amount = InvestmentLogs::where("user_id", "=", $user_id) -> sum('tc_amount');
        $interest_income = InterestLogs::where("user_id", "=", $user_id) -> where("is_withdrawed", "=", 1) -> sum('interest_amount');
        $investment_pullout = InvestmentLogs::where("user_id", "=", $user_id) -> where("is_withdrawed", "=", 1) -> sum('tc_amount');
        $withdrawed_amount = ConvertionLogs::where("user_id", "=", $user_id) -> sum("withdrawed_amount");
        $drBonus = DirectReferral::where("recruiter_id", "=", $user_id) -> where("is_withdrawed", "=", 1) -> sum("amount_earnings");

        return (($sum_amount - $invested_amount) + $interest_income + $investment_pullout + $drBonus) - $withdrawed_amount;
    }

    public function totalInvestments($user_id) {
        return InvestmentLogs::where("user_id", "=", $user_id) -> sum('tc_amount');
    }

    public function totalInterest($user_id) {
        return InterestLogs::where("user_id", "=", $user_id) -> sum('interest_amount');
    }

    public function totalDrBonus($user_id) {
        return DirectReferral::where("recruiter_id", "=", $user_id) -> sum("amount_earnings");
    }

    public function totalTCEarnings($user_id) {
        $sum_amount = PurchaseLogs::where("user_id", "=", $user_id) -> sum('tc_amount');
        $interest_income = InterestLogs::where("user_id", "=", $user_id) -> sum('interest_amount');
        $drBonus = DirectReferral::where("recruiter_id", "=", $user_id) -> sum("amount_earnings");
        $invested_amount = InvestmentLogs::where("user_id", "=", $user_id) -> sum('tc_amount');

        $total_earnings = ($sum_amount - $invested_amount) + $interest_income + $drBonus;
        $total_investments = InvestmentLogs::where("user_id", "=", $user_id) -> sum('tc_amount');

        return $total_investments + $total_earnings;
    }

    public function dueDate($start_date, $range) {
        $endDate = new \DateTime($start_date);
        $endDate -> modify(sprintf('+%s day', (int)$range));
        $endDay = $endDate -> format('Y-m-d H:i:s');

        return $endDay;
    }

    public function isCapitalLocked($trans_code, $lockin_range) {
        $investment = InvestmentLogs::where("trans_code", "=", $trans_code) -> where("is_withdrawed", "=", 0) -> first();
        if ($investment) {
            $endDate = new \DateTime($investment -> created_at);
            $endDate -> modify(sprintf('+%s day', (int)$lockin_range));
            $endDay = $endDate -> format('Y-m-d H:i:s');
            $today = date('Y-m-d');

            return ($today === $endDay) ? false : true;
        } else {
            return true;
        }
    }

    public function capitalDueDate($trans_code, $lockin_range) {
        $investment = InvestmentLogs::where("trans_code", "=", $trans_code) -> where("is_withdrawed", "=", 0) -> first();
        if ($investment) {
            $endDate = new \DateTime($investment -> created_at);
            $endDate -> modify(sprintf('+%s day', (int)$lockin_range));
            $endDay = $endDate -> format('Y-m-d H:i:s');

            return "Investment Capital can be pulled out on " . date("F j, Y", strtotime($endDay));
        } else {
            return "Transaction Completed";
        }
    }

    public function payoutStatus($link_id, $request_type) {
        $payout = Payouts::where("link_id", "=", $link_id) -> where("request_type", "=", $request_type) -> where("status", "=", 0) -> first();
        return ($payout) ? true : false;
    }

    public function countPayoutRequest() {
        return payouts::where("status", "=", 0) -> count();
    }

    public function interestEarnings($trans_code) {
        return InterestLogs::where("investment_code", "=", $trans_code) -> sum("interest_amount");
    }

    public function setDRBonus($dr, $user) {
        $dr_bonus = GeneralSettings::where("module_name", "=", "dr_bonus") -> first();
        $dr_bonus_amount = $dr_bonus -> content;

        $DR = new DirectReferral();
        $DR -> recruiter_id = $dr -> id;
        $DR -> recruitee_id = $user -> id;
        $DR -> amount_earnings = $dr_bonus_amount;
        $DR -> save();
    }

    public function processCompanyAllocatedIncome($user_id, $btc_amount) {
    }

    public function countPendingMessages($user_id) {
      return Messages::leftJoin("message_replies as MR", "MR.msg_ref_id", "=" , "messages.ref_id")
                          -> where("messages.to_user_id", "=", $user_id)
                          -> where("MR.is_read", "=", 0)
                          -> count();
    }

    public function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    public function sendMail($sendTo, $subject, $contentBody = "", $attachment = "") {
        try {
            // $_username = 'support@upnext.shop';
            $_username = EMAIL_USERNAME;

            $mail = new \PHPMailer;

            $mail -> SMTPDebug = 0;

            $mail -> isSMTP();
            $mail -> Host = 'smtp.gmail.com';
            $mail -> SMTPAuth = true;
            $mail -> Username = EMAIL_USERNAME;
            $mail -> Password = EMAIL_PASSWORD;
            $mail -> SMTPSecure = 'ssl';
            $mail -> Port = 465;

            $mail -> setFrom($_username, '[ACES COIN] - No reply');
            $mail -> addAddress($sendTo);
            // $mail -> addAddress('ellen@example.com');
            // $mail -> addReplyTo('info@example.com', 'Information');

            // $mail -> addCC('upnextwebmaster@gmail.com');
            // $mail -> addBCC('ullen.d.faustino@gmail.com');

            // if (is_array($attachment)) {
            // 	if (count($attachment) > 0) {
            // 		foreach ($attachment as $key => $item) {
            // 			$mail -> addAttachment($item);
            // 		}
            // 	}
            // } else {
            // 	if (strlen($attachment) > 0) {
            // 		$mail -> addAttachment($attachment);
            // 	}
            // }
            $mail -> isHTML(true);

            $mail -> Subject = $subject;
            $mail -> Body = $contentBody;
            // $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';
            if (!$mail -> send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail -> ErrorInfo;
                return 'Mailer Error: ' . $mail -> ErrorInfo;
            } else {
                echo 'Message has been sent';
                return true;
            }
        } catch(\Exception $e) {
            echo $e -> getMessage();
            return 'Mailer Error: ' . $e -> getMessage();
        }
    }

    public function sendAccountVerification($user, $activationCode, $password) {
        if (SEND_EMAIL) {
            $to = $user -> email;
            $subject = "[ACES COIN] Account Verification";
            $body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to ACES COIN.</b><p>";
            $body .= "<p>";
            $body .= "<span>Hi </span>: <b>" . $user -> full_name . "</b>";
            $body .= "</p>";
            $body .= "<p>";
            $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
            $body .= "</p>";
            $body .= "<p>";
            $body .= "<span>Password</span>: <b>" . $password . "</b>";
            $body .= "</p>";
            $body .= "<p>";
            $body .= "<span>Pin Code</span>: <b>" . $user -> pin_code . "</b>";
            $body .= "</p>";
            if ($user -> package_type == 3) {
                $body .= sprintf("<p>Website Url: %s</p>", GenericHelper::baseUrl());
            } else {
                $body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', GenericHelper::baseUrl(), $user -> id, $activationCode) . "</b></p>";
            }

            $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body));
            // echo $mail_cmd;exit;
            pclose(popen($mail_cmd, "w"));

            return true;
        } else {
            return false;
        }
    }

    public function sendRawEmail($to, $subject, $body) {
        if (SEND_EMAIL) {
            $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body));
            pclose(popen($mail_cmd, "w"));
            return true;
        } else {
            return false;
        }
    }

    public function resendEmailVerification($user) {
        if (SEND_EMAIL) {
            $to = $user -> email;

            $subject = "[ACES COIN] Account Verification";
            $body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to ACES COIN.</b><p>";
            $body .= "<p>";
            $body .= "<span>Hi </span>: <b>" . $user -> full_name . "</b>";
            $body .= "</p>";
            $body .= "<p>";
            $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
            $body .= "</p>";
            $body .= "<p>";
            $body .= "<span>Password</span>: <b>" . base64_decode($user -> canonical_hash) . "</b>";
            $body .= "</p>";
            $body .= "<p>";
            $body .= "<span>Pin Code</span>: <b>" . $user -> pin_code . "</b>";
            $body .= "</p>";
            if ($user -> package_type == 3) {
                $body .= sprintf("<p>Website Url: %s</p>", GenericHelper::baseUrl());
            } else {
                $body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', GenericHelper::baseUrl(), $user -> id, $user -> activation_code) . "</b></p>";
            }

            $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body));
            pclose(popen($mail_cmd, "w"));

            return true;
        } else {
            return false;
        }
    }

    public function baseUrl() {
        // $path = dirname($_SERVER['SCRIPT_NAME']);
        // $path = trim($path, '/');
        // $baseUrl = Request::getUrl();
        // $baseUrl = trim($baseUrl, '/');
        // return $baseUrl . '/' . $path . ($path ? '/' : '');
        return _BASE_URL . "/";
    }

    public function dirToArray($dir) {

        $result = array();

        $cdir = scandir($dir);
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                    $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
                } else {
                    if ($value !== "default-avatar.png") {
                        $result[] = $value;
                    }
                }
            }
        }

        return $result;
    }

}
