<?php
namespace SlimStarter\Module;

interface ModuleInterface{
    public function getModuleName();
    public function getModuleAccessor();
    public function getTemplatePath();
    public function registerAdminRoute();
    public function registerMemberRoute();
	public function registerCashierRoute();
    public function registerAdminMenu();
    public function registerMemberMenu();
	public function registerCashierMenu();
    public function registerHook();
    public function boot();
    public function install();
    public function uninstall();
    public function activate();
    public function deactivate();
}