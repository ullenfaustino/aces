<?php

namespace SlimStarter\TwigExtension;
use \Slim;

use \Sentry;
use \User;
use \Users;
use \GeneralSettings;
use \GenericHelper;

class Service extends \Twig_Extension
{
    public function getName()
    {
        return 'service';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getUserdetails', array($this, 'getUserdetails')),
          	new \Twig_SimpleFunction('timeAgo', array($this, 'timeAgo')),
            new \Twig_SimpleFunction('convertToAUD_BUY', array($this, 'convertToAUD_BUY')),
            new \Twig_SimpleFunction('convertToAUD_SELL', array($this, 'convertToAUD_SELL')),
            new \Twig_SimpleFunction('convertTC_BTC_SELL', array($this, 'convertTC_BTC_SELL')),
            new \Twig_SimpleFunction('convertTC_BTC_BUY', array($this, 'convertTC_BTC_BUY')),
            new \Twig_SimpleFunction('convertTC_AUD_SELL', array($this, 'convertTC_AUD_SELL')),
            new \Twig_SimpleFunction('totalTCEarnings', array($this, 'totalTCEarnings')),
            new \Twig_SimpleFunction('dueDate', array($this, 'dueDate')),
            new \Twig_SimpleFunction('interestEarnings', array($this, 'interestEarnings')),
            new \Twig_SimpleFunction('availableCoinBalance', array($this, 'availableCoinBalance')),
            new \Twig_SimpleFunction('totalInvestments', array($this, 'totalInvestments')),
            new \Twig_SimpleFunction('totalInterest', array($this, 'totalInterest')),
            new \Twig_SimpleFunction('isCapitalLocked', array($this, 'isCapitalLocked')),
            new \Twig_SimpleFunction('payoutStatus', array($this, 'payoutStatus')),
            new \Twig_SimpleFunction('countPayoutRequest', array($this, 'countPayoutRequest')),
            new \Twig_SimpleFunction('capitalDueDate', array($this, 'capitalDueDate')),
            new \Twig_SimpleFunction('countPendingMessages', array($this, 'countPendingMessages')),
			);
    }

    public function countPendingMessages($user_id) {
      return GenericHelper::countPendingMessages($user_id);
    }

    public function timeAgo($date) {
    	return GenericHelper::timeAgo($date);
    }

    public function getUserdetails($id) {
        return Users::find($id);
    }

    public function convertToAUD_BUY($coin) {
      return GenericHelper::convertToAUD_BUY($coin);
    }

    public function convertToAUD_SELL($coin) {
      return GenericHelper::convertToAUD_SELL($coin);
    }

    public function convertTC_BTC_BUY($coin) {
      return GenericHelper::convertTC_BTC_BUY($coin);
    }

    public function convertTC_BTC_SELL($coin) {
      return GenericHelper::convertTC_BTC_SELL($coin);
    }

    public function convertTC_AUD_SELL($coin) {
      return GenericHelper::convertTC_AUD_SELL($coin);
    }

    public function totalTCEarnings($user_id) {
      return GenericHelper::totalTCEarnings($user_id);
    }

    public function dueDate($start_date, $range) {
      return GenericHelper::dueDate($start_date, $range);
    }

    public function interestEarnings($trans_code) {
      return GenericHelper::interestEarnings($trans_code);
    }

    public function availableCoinBalance($user_id) {
      return GenericHelper::availableCoinBalance($user_id);
    }

    public function totalInvestments($user_id) {
      return GenericHelper::totalInvestments($user_id);
    }

    public function totalInterest($user_id) {
      return GenericHelper::totalInterest($user_id);
    }

    public function isCapitalLocked($trans_code, $lockin_range) {
  		return GenericHelper::isCapitalLocked($trans_code, $lockin_range);
  	}

    public function payoutStatus($link_id, $request_type) {
  		return GenericHelper::payoutStatus($link_id, $request_type);
  	}

    public function countPayoutRequest() {
  		return GenericHelper::countPayoutRequest();
  	}

    public function capitalDueDate($trans_code, $lockin_range) {
      return GenericHelper::capitalDueDate($trans_code, $lockin_range);
    }
}
