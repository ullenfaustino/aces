<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class MAILNOTIFICATION extends BaseController {

    function __construct() {}

    public function SendMail($to, $subject, $body) {
    	$subject = base64_decode($subject);
    	$body = base64_decode($body);
    	GenericHelper::sendMail($to, $subject, $body);
	}

}

$notif = new MAILNOTIFICATION();
$notif -> SendMail($argv[1], $argv[2], $argv[3]);

?>
