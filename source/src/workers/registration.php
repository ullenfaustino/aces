<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class REGISTRATION extends BaseController {

    function __construct() {}

    public function register($credential, $dr) {
      $credential = json_decode(base64_decode($credential), TRUE);

      $user = Sentry::register($credential, false);
      $user -> addGroup(Sentry::getGroupProvider() -> findByName('Member'));

      // ASSOCIATE WALLET IN PACKAGE
      $newWallet_package = GenericHelper::generateNewAddress(BLOCKTRAIL_WALLET_USERNAME, BLOCKTRAIL_WALLET_PASSWORD);
      $wallet = $newWallet_package['address'];

      $user -> associated_wallet = $wallet;
      if ($user -> save()) {
        // set DR
        if ($dr !== "wala") {
            $dr_user = Users::where("username", "=", $dr) -> where("user_type", "=", 3) -> where("activated","=",1) -> first();
            if ($dr_user) {
                GenericHelper::setDRBonus($dr_user, $user);
            }
        }
        
        //set wallet webhook
        GenericHelper::setWalletWebHook($wallet, $user);
      }

      $activationCode = $user -> getActivationCode();
      GenericHelper::sendAccountVerification($user, $activationCode, $credential['password']);
	}

}

$reg = new REGISTRATION();
$reg -> register($argv[1], $argv[2]);

?>
