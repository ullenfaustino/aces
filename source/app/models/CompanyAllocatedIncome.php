<?php
use Illuminate\Database\Eloquent\Model as Model;

class CompanyAllocatedIncome extends Model {
	protected $table = "company_allocated_income";
}