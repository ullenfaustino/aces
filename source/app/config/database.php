<?php

$configINI = parse_ini_file(__DIR__ . "/../config.ini", TRUE);
$config['database'] = array(
    'default'       => 'mysql',

    'connections'   => array(

        'mysql' => array(
            'driver'    => 'mysql',
            'host'      => isset($_SERVER['DB1_HOST']) ? $_SERVER['DB1_HOST'] : $configINI['database']['host'],
            'database'  => isset($_SERVER['DB1_NAME']) ? $_SERVER['DB1_NAME'] : $configINI['database']['dbname'],
            'username'  => isset($_SERVER['DB1_USER']) ? $_SERVER['DB1_USER'] : $configINI['database']['username'],
            'password'  => isset($_SERVER['DB1_PASS']) ? $_SERVER['DB1_PASS'] : $configINI['database']['password'],
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

        'sqlite' => array(
            'driver'   => 'sqlite',
            'database' => APP_PATH.'storage/db/database.sqlite',
            'prefix'   => '',
        ),

        'pgsql' => array(
            'driver'   => 'pgsql',
            'host'     => 'localhost',
            'database' => 'database',
            'username' => 'root',
            'password' => '',
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public',
        ),

        'sqlsrv' => array(
            'driver'   => 'sqlsrv',
            'host'     => '127.0.0.1',
            'database' => 'database',
            'username' => 'user',
            'password' => '',
            'prefix'   => '',
        ),

    )
);
