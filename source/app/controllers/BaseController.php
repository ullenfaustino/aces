<?php

class BaseController
{

    protected $app;
    protected $data;

    public function __construct()
    {
        $this->app = Slim\Slim::getInstance();
        $this->data = array();

        /** default title */
        $this->data['title'] = '';

        /** meta tag and information */
        $this->data['meta'] = array();

        /** queued css files */
        $this->data['css'] = array(
            'internal'  => array(),
            'external'  => array()
        );

        /** queued js files */
        $this->data['js'] = array(
            'internal'  => array(),
            'external'  => array()
        );

        /** prepared message info */
        $this->data['message'] = array(
            'error'    => array(),
            'info'    => array(),
            'debug'    => array(),
        );

        /** global javascript var */
        $this->data['global'] = array();

        /** base dir for asset file */
        $this->data['avatarUrl']  = AVATAR_PATH;
        $this->data['flagUrl']  = FLAG_PATH;
        $this->data['baseUrl']  = $this->baseUrl();
        $this->data['assetUrl'] = $this->data['baseUrl'].'assets/';
        $this->data['request_uri'] = $_SERVER['REQUEST_URI'];

        if (Sentry::check()) {
        	$user = Sentry::getUser();
          $this -> data['user'] = $user;

          $buy_value = GeneralSettings::where("module_name","=","buy_value") -> first();
          $buy_value_btc = GeneralSettings::where("module_name","=","buy_value_btc") -> first();
          $sell_value = GeneralSettings::where("module_name","=","sell_value") -> first();
          $sell_value_btc = GeneralSettings::where("module_name","=","sell_value_btc") -> first();
          $this -> data['coin']['buy_value'] = $buy_value -> content;
          $this -> data['coin']['sell_value'] = $sell_value -> content;
          $this -> data['coin']['buy_value_btc'] = $buy_value_btc -> content;
          $this -> data['coin']['sell_value_btc'] = $sell_value_btc -> content;
        }

        // package 1
        $package_1_range = GeneralSettings::where("module_name","=","package_1_range") -> first();
        $this -> data['package_1_range'] = $package_1_range -> content;

        $package_1_interest = GeneralSettings::where("module_name","=","package_1_interest") -> first();
        $this -> data['package_1_interest'] = $package_1_interest -> content;

        $package_1_lockin = GeneralSettings::where("module_name","=","package_1_lockin") -> first();
        $this -> data['package_1_lockin'] = $package_1_lockin -> content;

        // package 2
        $package_2_range = GeneralSettings::where("module_name","=","package_2_range") -> first();
        $this -> data['package_2_range'] = $package_2_range -> content;

        $package_2_interest = GeneralSettings::where("module_name","=","package_2_interest") -> first();
        $this -> data['package_2_interest'] = $package_2_interest -> content;

        $package_2_lockin = GeneralSettings::where("module_name","=","package_2_lockin") -> first();
        $this -> data['package_2_lockin'] = $package_2_lockin -> content;

        $dr_bonus = GeneralSettings::where("module_name","=","dr_bonus") -> first();
        $this -> data['dr_bonus'] = $dr_bonus -> content;

        // Admin Control
        $registration = GeneralSettings::where("module_name","=","registration") -> first();
        $this -> data['registration'] = $registration -> content;

        $payout = GeneralSettings::where("module_name","=","payout") -> first();
        $this -> data['payout'] = $payout -> content;

        $this -> setLandingPageData();
    }

    private function setLandingPageData() {
      $description = GeneralSettings::where('module_name','=','landing_description') -> first();
      $this -> data['landing']['description'] = ($description) ? $description -> content : "";

      $about = GeneralSettings::where('module_name','=','landing_about') -> first();
      $this -> data['landing']['about'] = ($about) ? $about -> content : "";

      $mission = GeneralSettings::where('module_name','=','landing_mission') -> first();
      $this -> data['landing']['mission'] = ($mission) ? $mission -> content : "";

      $vision = GeneralSettings::where('module_name','=','landing_vision') -> first();
      $this -> data['landing']['vision'] = ($vision) ? $vision -> content : "";

      $address = GeneralSettings::where('module_name','=','landing_address') -> first();
      $this -> data['landing']['address'] = ($address) ? $address -> content : "";

      $lat = GeneralSettings::where('module_name','=','landing_lat') -> first();
      $this -> data['landing']['lat'] = ($lat) ? $lat -> content : "#";

      $lng = GeneralSettings::where('module_name','=','landing_lng') -> first();
      $this -> data['landing']['lng'] = ($lng) ? $lng -> content : "#";

      $phone = GeneralSettings::where('module_name','=','landing_phone') -> first();
      $this -> data['landing']['phone'] = ($phone) ? $phone -> content : "";

      $email = GeneralSettings::where('module_name','=','landing_email') -> first();
      $this -> data['landing']['email'] = ($email) ? $email -> content : "";

      $facebook = GeneralSettings::where('module_name','=','landing_facebook') -> first();
      $this -> data['landing']['facebook'] = ($facebook) ? $facebook -> content : "#";

      $twitter = GeneralSettings::where('module_name','=','landing_twitter') -> first();
      $this -> data['landing']['twitter'] = ($twitter) ? $twitter -> content : "#";

      $google = GeneralSettings::where('module_name','=','landing_google') -> first();
      $this -> data['landing']['google'] = ($google) ? $google -> content : "#";
    }

    /**
     * addMessage to be viewd in the view file
     */
    protected function message($message, $type='info')
    {
        $this->data['message'][$type] = $message;
    }

    /**
     * register global variable to be accessed via javascript
     */
    protected function publish($key,$val)
    {
        $this->data['global'][$key] =  $val;
    }

    /**
     * remove published variable from registry
     */
    protected function unpublish($key)
    {
        unset($this->data['global'][$key]);
    }

    /**
     * add custom meta tags to the page
     */
    protected function meta($name, $content)
    {
        $this->data['meta'][$name] = $content;
    }

    /**
     * generate base URL
     */
    protected function baseUrl()
    {
        $path       = dirname($_SERVER['SCRIPT_NAME']);
        $path       = trim($path, '/');
        $baseUrl    = Request::getUrl();
        $baseUrl    = trim($baseUrl, '/');
        return $baseUrl.'/'.$path.( $path ? '/' : '' );
    }

    /**
     * generate siteUrl
     */
    protected function siteUrl($path, $includeIndex = false)
    {
        $path = trim($path, '/');
        return $this->data['baseUrl'].$path;
    }

    public function facebook() {
        return new \Facebook\Facebook( array('app_id' => FB_APP_ID, 'app_secret' => FB_APP_SECRET, 'default_graph_version' => 'v2.7', ));
    }

    public function setupDynamicValues() {
  		// landing page
  		$landing_page = GeneralSettings::where('module_name', '=', 'login_attribute') -> first();
  		if ($landing_page) {
  			$this -> data['login_attribute'] = json_decode($landing_page -> content);
  		}

  		// visit us
          $visit_us = GeneralSettings::where('module_name', '=', 'visit_us') -> first();
          if ($visit_us) {
              $this -> data['visit_us'] = json_decode($visit_us -> content);
          }

          // contacts
  		$contacts = GeneralSettings::where('module_name', '=', 'contacts') -> first();
  		if ($contacts) {
  			$this -> data['contacts'] = json_decode($contacts -> content);
  		}

  		// about
  		$about = GeneralSettings::where('module_name', '=', 'about') -> first();
  		if ($about) {
  			$this -> data['about'] = json_decode($about -> content);
  		}
  	}
}
