<?php

use \Illuminate\Database\Capsule\Manager as DB;
use \Blocktrail\SDK\BlocktrailSDK;

class APIController extends BaseController {

    public function testMailer() {
        $sendTo = "ullen.d.faustino@gmail.com";
        $subject = "[TESTER] test mailer.";
        $contentBody = "<p>test mail successfull.</p>";
        // GenericHelper::sendMail($sendTo, $subject, $contentBody);

        $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $sendTo, base64_encode($subject), base64_encode($contentBody));
        pclose(popen($mail_cmd, "w"));
    }

    public function companyWalletDetails() {
      $details = GenericHelper::getWalletBalance(BLOCKTRAIL_WALLET_USERNAME, BLOCKTRAIL_WALLET_PASSWORD);

      $details['tc_value'] = GenericHelper::convertBTC_TC_BUY($details['confirmed_balance']);
      $details['aud_value'] = GenericHelper::convertToAUD_BUY($details['tc_value']);

      Response::headers() -> set('Content-Type', 'application/json');
      Response::setBody(json_encode($details));
    }

    public function addDummyAccounts() {
        $flags = GenericHelper::dirToArray(FLAGS_DIR);
        $avatar = $flags[array_rand($flags)];
        $codeGen = new CodeGenerator();

        $dummyAccounts = new DummyAccounts();
        $dummyAccounts -> ref_code = $codeGen -> getToken(42);
        $dummyAccounts -> username = $this -> generateUsername();
        $dummyAccounts -> avatar = $avatar;
        $dummyAccounts -> invested_amount = rand(1,100);
        $dummyAccounts -> withdrawed_amount = rand(1,($dummyAccounts -> invested_amount / 2));
        $dummyAccounts -> save();

        Response::headers() -> set('Content-Type', 'application/json');
        Response::setBody($dummyAccounts);
    }

    private function generateUsername() {
        $codeGen = new CodeGenerator();
        $gen_username = sprintf("TI%s", $codeGen -> getNumericToken(8));
        $username = Users::where('username', '=', $gen_username) -> first();
        if (!$username) {
            return $gen_username;
        } else {
            return $this -> generateUsername();
        }
    }

    public function verifyPincode($pincode) {
        $user = Sentry::getUser();

        $isValid = false;
        if ($user -> pin_code == $pincode) {
            $isValid = true;
        }

        Response::headers() -> set('Content-Type', 'application/json');
        Response::setBody($isValid);
    }

    public function convertToBTC($tc_amount) {
        $sell_value_btc = GeneralSettings::where("module_name", "=", "sell_value_btc") -> first();
        $value = (double)$sell_value_btc -> content * (double)$tc_amount;

        Response::headers() -> set('Content-Type', 'application/json');
        Response::setBody($value);
    }

    public function convertToAUD($tc_amount) {
        $sell_value = GeneralSettings::where("module_name", "=", "sell_value") -> first();
        $value = (double)$sell_value -> content * (double)$tc_amount;

        $value = number_format ( $value, 2, '.', ',' );

        Response::headers() -> set('Content-Type', 'application/json');
        Response::setBody($value);
    }

    public function convertBTC_TC($btc) {
      $sell_value_btc = GeneralSettings::where("module_name", "=", "sell_value_btc") -> first();
      $value = (double)$btc / (double)$sell_value_btc -> content;

      $value = number_format ( $value, 2, '.', ',' );

      Response::headers() -> set('Content-Type', 'application/json');
      Response::setBody($value);
    }

    public function convertAUD_TC($aud) {
      $sell_value = GeneralSettings::where("module_name", "=", "sell_value") -> first();
      $value = (double)$aud / (double)$sell_value -> content;

      $value = number_format ( $value, 2, '.', ',' );

      Response::headers() -> set('Content-Type', 'application/json');
      Response::setBody($value);
    }

    // buy converter
    public function convertToBTC_BUY($tc_amount) {
        $sell_value_btc = GeneralSettings::where("module_name", "=", "buy_value_btc") -> first();
        $value = (double)$sell_value_btc -> content * (double)$tc_amount;

        Response::headers() -> set('Content-Type', 'application/json');
        Response::setBody($value);
    }

    public function convertToAUD_BUY($tc_amount) {
        $sell_value = GeneralSettings::where("module_name", "=", "buy_value") -> first();
        $value = (double)$sell_value -> content * (double)$tc_amount;

        $value = number_format ( $value, 2, '.', ',' );

        Response::headers() -> set('Content-Type', 'application/json');
        Response::setBody($value);
    }

    public function convertBTC_TC_BUY($btc) {
      $sell_value_btc = GeneralSettings::where("module_name", "=", "buy_value_btc") -> first();
      $value = (double)$btc / (double)$sell_value_btc -> content;

      $value = number_format ( $value, 2, '.', ',' );

      Response::headers() -> set('Content-Type', 'application/json');
      Response::setBody($value);
    }

    public function convertAUD_TC_BUY($aud) {
      $sell_value = GeneralSettings::where("module_name", "=", "buy_value") -> first();
      $value = (double)$aud / (double)$sell_value -> content;

      $value = number_format ( $value, 2, '.', ',' );

      Response::headers() -> set('Content-Type', 'application/json');
      Response::setBody($value);
    }

    public function webhookHandler($wallet_address) {
        try {
            $payload = BlocktrailSDK::getWebhookPayload();

            if ($payload['event_type'] === "address-transactions") {
                $hashCode = $payload['data']['hash'];
                $address_received_value = $payload['addresses'][$wallet_address];

                $user = Users::where('associated_wallet', '=', $wallet_address) -> first();
                if ($user && ($address_received_value > 0)) {
                    $btc_amount = GenericHelper::decodeSatoshi($address_received_value);
                    $tc_amount = GenericHelper::convertBTC_TC_BUY($btc_amount);

                    $logs = PurchaseLogs::where("hash","=",$hashCode)->first();
                    if (!$logs) {
                      $logs = new PurchaseLogs();
                      $logs -> hash = $hashCode;
                      $logs -> address = $wallet_address;
                      $logs -> user_id = $user -> id;
                      $logs -> btc_amount = $btc_amount;
                      $logs -> tc_amount = $tc_amount;
                      $logs -> save();
                    }

                    $subject = "[Cryptotytanium Coin] Investment Received from " . $wallet_address . " , trx. code: " . $hashCode . $contentBody = "<p>You have successfully received " . $btc_amount . " BTC.</p>";
                    $this -> sendEmailNotificationReceivedIntoWallet($subject, $contentBody, $user -> email);
                }
            }
        } catch(\Exception $e) {
            echo $e -> getMessage();
            $subject = "Error on webhook handler on blocktrail";
            $contentBody = "<p>Error message : " . $e -> getMessage() . "</p>";
            $this -> sendEmailNotificationReceivedIntoWallet($subject, $contentBody);
        }

    }

    public function checkInvestmentsValidity() {
        $investments = InvestmentLogs::where('is_withdrawed', '=', 0) -> orderBy('created_at', 'desc') -> get();

        foreach ($investments as $key => $investment) {
            $range = $investment -> days_range;

            $endDate = new \DateTime($investment -> created_at);
            $endDate -> modify(sprintf('+%s day', (int)$range));
            $endDay = $endDate -> format('Y-m-d');

            $today = date('Y-m-d');

            if ($today === $endDay) {
                if ($investment -> is_due == 0) {
                    $investment -> is_due = 1;
                    $investment -> save();

                    // send email notification to member
                    $this -> sendInvestmentDueNotification($investment);
                }
            }
        }

        Response::headers() -> set('Content-Type', 'application/json');
        Response::setBody(json_encode(array('status' => 'Success')));
    }

    public function processInvestments() {
        $investments = InvestmentLogs::where('is_withdrawed', '=', 0) -> where('is_due', '=', 0) -> orderBy('created_at', 'desc') -> get();

        foreach ($investments as $key => $investment) {
            $interest_rate = $investment -> interest;
            $percent_rate = $interest_rate / 100;
            $currentBalance = $investment -> tc_amount;

            $interest_amount = $currentBalance * $percent_rate;

            $interest_logs = new InterestLogs();
            $interest_logs -> user_id = $investment -> user_id;
            $interest_logs -> investment_code = $investment -> trans_code;
            $interest_logs -> current_balance = $currentBalance;
            $interest_logs -> interest_amount = $interest_amount;
            $interest_logs -> interest_rate = $interest_rate;
            $interest_logs -> save();
        }

        Response::headers() -> set('Content-Type', 'application/json');
        Response::setBody(json_encode(array('status' => 'Success')));
    }

    private function sendEmailNotificationReceivedIntoWallet($subject, $contentBody, $sendTo = null) {
        if (is_null($sendTo)) {
            $sendTo = "ullen.d.faustino@gmail.com";
        }
        if (SEND_EMAIL) {
            $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $sendTo, base64_encode($subject), base64_encode($contentBody));
            pclose(popen($mail_cmd, "w"));
        }
    }

    private function sendInvestmentDueNotification($investment) {
        $member = Users::find($investment -> user_id);
        if ($member) {
            $subject = "[Bitomi] Encashment is now available.";
            $contentBody = "<p>Congratulations! Your investment with transaction code " . $investment -> trans_code . " is now available to encash to bitcoin.</p>";
            $contentBody .= "<p>click here to view transaction </br>" . "<b>" . sprintf('%smember/transaction/view/%s', $this -> baseUrl(), $investment -> trans_code) . "</b></p>";
            $this -> sendEmailNotificationReceivedIntoWallet($subject, $contentBody, $member -> email);
        }
    }

}
