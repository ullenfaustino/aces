<?php

class RegistrationController extends BaseController {

    public function registration_view() {
      $this -> data['title'] = 'ACES - Registration';
        $this -> data['generated_username'] = $this -> generateUsername();
        $this -> data['flags'] = GenericHelper::dirToArray(FLAGS_DIR);
	    // $this -> setupDynamicValues();
			View::display('user/registration.twig', $this -> data);
    }

    public function registration_view_dr($direct_referral) {
      $is_valid_dr = Users::where("username", "=", $direct_referral) -> where("user_type", "=", 3) -> first();
      if ($is_valid_dr) {
          $this -> data['title'] = 'ACES - Registration';
          $this -> data['generated_username'] = $this -> generateUsername();
          $this -> data['flags'] = GenericHelper::dirToArray(FLAGS_DIR);
          $this -> data['direct_referral'] = $direct_referral;
          // $this -> setupDynamicValues();
          View::display('user/registration.twig', $this -> data);
      } else {
          Response::redirect($this -> siteUrl("/registration"));
      }
    }

    public function pre_registration() {
        $codeGen = new CodeGenerator();

        $ref_id = $this -> genRefID();
        $username = Input::post('username');
        $email = Input::post('email');
        $fullname = Input::post('full_name');
        $pin_code = Input::post('pin_code');
        $avatar = Input::post("avatar");
        $direct_referral = Input::post("direct_referral");
        try {
          $randomPassword = $codeGen -> getToken(12);
          $credential = ['ref_id' => $ref_id,
      					'username' => trim($username),
      					'email' => trim($email),
      					'password' => $randomPassword,
      					'first_name' => trim($fullname),
      					'avatar' => $avatar,
      					'activated' => 0,
      					'user_type' => 3,
      					'is_registered' => 1,
      					'pin_code' => $pin_code,
      					'permissions' => array('member' => 1)
          ];

          $dr = ($direct_referral) ? $direct_referral : "wala";

          $reg_cmd = sprintf("php %s/registration.php %s %s &", WORKERS_PATH, base64_encode(json_encode($credential)), $dr);
		      pclose(popen($reg_cmd, "w"));

          App::flash('message_status', true);
          App::flash('message', "Check your email to verify your account.");
      } catch ( \Cartalyst\Sentry\Users\UserNotFoundException $e ) {
          App::flash('message_status', false);
          App::flash('message', "Sponsor must exist! Please select valid direct.");
      } catch (\Exception $e ) {
          App::flash('message_status', false);
          App::flash('message', $e -> getMessage());
      }
      Response::redirect($this -> siteUrl('/login'));
  }

  private function sendAccountVerification($user, $activationCode, $password) {
      $to = $user -> email;
      $subject = "[Bitomi] Account Verification";
      $body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to Bitomi.</b><p>";
      $body .= "<p>";
      $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
      $body .= "</p>";
      $body .= "<p>";
      $body .= "<span>Password</span>: <b>" . $password . "</b>";
      $body .= "</p>";
      $body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', $this -> data['baseUrl'], $user -> username, $activationCode) . "</b></p>";
      GenericHelper::sendMail($to, $subject, $body);
  }

  private function generateUsername() {
      $codeGen = new CodeGenerator();
      $gen_username = sprintf("ACE%s", $codeGen -> getNumericToken(8));
      $username = Users::where('username', '=', $gen_username) -> first();
      if (!$username) {
          return $gen_username;
      } else {
          return $this -> generateUsername();
      }
  }

  private function genRefID() {
      $codeGen = new CodeGenerator();
      $uuid = $codeGen -> getToken(12);
      $refid = Users::where('ref_id', '=', $uuid) -> first();
      if (!$refid) {
          return $uuid;
      } else {
          return $this -> genRefID();
      }
  }

  public function confirmation($username, $activation_code) {
      $this -> data['title'] = 'ACES Coin - Account Details';
      try {
          // Find the user using the user id
          $user = Sentry::findUserById($username);

          // Check if the user is activated or not
          if ($user -> isActivated()) {
              Response::redirect($this -> siteUrl('/'));
              return;
          } else {
              // Attempt to activate the user
              if ($user -> attemptActivation($activation_code)) {
                  Sentry::login($user, false);
              } else {
                  App::flash('message_status', false);
                  App::flash('message', 'Failed to activate User account.');
              }
              Response::redirect($this -> siteUrl('/'));
          }
      } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
          App::flash('message_status', false);
          App::flash('message', 'User was not found.');
      }
      // View::display('user/reg_confirmation.twig', $this -> data);
      Response::redirect($this -> siteUrl('/'));
  }

}
