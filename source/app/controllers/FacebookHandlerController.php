<?php

class FacebookHandlerController extends BaseController {

    public function FBCallbackHandler() {
        $helper = $this -> facebook() -> getRedirectLoginHelper();
        try {
            $accessToken = $helper -> getAccessToken();
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e -> getMessage();
            exit ;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e -> getMessage();
            exit ;
        }
        if (isset($accessToken)) {
            // echo "access token | " . $accessToken;
            $_SESSION['facebook_access_token'] = (string)$accessToken;
            if (FacebookHelper::shareOnFacebook()) {
            	$codeGen = new CodeGenerator();
            	$user = Sentry::getUser();
            	
                // Manual Mining earnings
                $manual_mining_earnings = 0;
                $manual_mining = GeneralSettings::where('module_name', '=', 'manual_mining') -> first();
                if ($manual_mining) {
                    $mm = json_decode($manual_mining -> content);
                    $manual_mining_earnings = $mm -> value;
                }
            	
            	$user -> share_balance += $manual_mining_earnings;
            	$user -> save();
            	
            	$trans = new ManualMiningLogs();
            	$trans -> ref_code = $codeGen -> getToken(6);
            	$trans -> user_id = $user -> id;
                $trans -> amount = $manual_mining_earnings;
                $trans -> save();
            }
        }
        Response::redirect($this -> siteUrl('member/mining'));
    }
}
