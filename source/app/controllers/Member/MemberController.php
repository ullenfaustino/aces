<?php

namespace Member;

use \App;
use \View;
use \Input;
use \Sentry;
use \Menu;
use \Response;
use \Users;
use \User;
use \GenericHelper;

class MemberController extends BaseController {

    /**
     * display the member dashboard
     */
    public function index() {
        $user = Sentry::getUser();
        if (Sentry::check()) {
            $this -> data['user'] = $user;
        }

        if ($user -> is_registered == 1) {
            if ($user -> is_password_changed == 0) {
                Response::Redirect($this -> siteUrl('member/set/password/' . base64_encode($user -> id)));
                return;
            }
            Response::Redirect($this -> siteUrl('member/transaction'));
        } else {
            Response::Redirect($this -> siteUrl('member/activation'));
        }
    }

    public function accountActivation() {
        Menu::get('member_sidebar') -> setActiveMenu('activation');

        $user = Sentry::getUser();
        $this -> data['title'] = "Activate Your Account";

        $allUsers = User::where('user_type', '<>', 1) -> where('user_type', '<>', 2) -> where('is_registered', '=', 1) -> where('id', '<>', $user -> id) -> get();

        $this -> data['all_users'] = $allUsers;
        View::display('member/index.twig', $this -> data);
    }

    public function setMemberPassword($id) {
        $id = base64_decode($id);
        $user = Users::find($id);
        $this -> data['user'] = $user;
        $this -> data['title'] = '[ACES] - Set Your New Password';
        View::display('member/setpassword.twig', $this -> data);
    }

    public function setMemberNewPassword() {
        $password = Input::post("password");
        $confirm = Input::post("confirm");
        $user_id = Input::post("user_id");

        if ($confirm !== $password) {
          App::flash('message_status', false);
          App::flash('message', "Password does not match!");
          Response::Redirect($this -> siteUrl('member/set/password/' . base64_encode($user_id)));
          return;
        }

        try {
          $user = Sentry::findUserById($user_id);
          $user -> password = $password;
          $user -> is_password_changed = 1;
          $user -> save();

          $this -> sendChangePasswordNotification($user, $password);

          App::flash('message_status', true);
          App::flash('message', "Alright!");

          Response::Redirect($this -> siteUrl('member'));
        } catch (\Exception $e) {
          App::flash('message_status', false);
          App::flash('message', $e -> getMessage());
          Response::Redirect($this -> siteUrl('member/set/password/' . base64_encode($user_id)));
        }
    }

    public function setMemberCurrentPassword() {
        $user = Sentry::getUser();
        $password = Input::post("confirm");
        $currentPassword = Input::post("current_password");

        try {
            $user = Sentry::findUserById($user -> id);

            if ($user -> checkPassword($currentPassword)) {
                $user -> password = $password;
                $user -> is_password_changed = 1;
                $user -> save();

                $this -> sendChangePasswordNotification($user, $password);

                App::flash('message_status', true);
                App::flash('message', "Password successfully updated.");
            } else {
                App::flash('message_status', false);
                App::flash('message', "Password does not match.");
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            App::flash('message_status', false);
            App::flash('message', $e -> getMessage());
        }
        Response::Redirect($this -> siteUrl('member/profile'));
    }

    private function sendChangePasswordNotification($user, $password) {
        $to = $user -> email;
        $subject = "[ACES] Change Password Confirmation";
        $body = "<p style='color: blue; font-weight: bold;'><b>Your Password has been successfully Changed.</b><p>";
        $body .= "<p>";
        $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
        $body .= "</p>";
        $body .= "<p>";
        $body .= "<span>Password</span>: <b>" . $password . "</b>";
        $body .= "</p>";
        // GenericHelper::sendMail($to, $subject, $body);

        if (SEND_EMAIL) {
          $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body));
    			pclose(popen($mail_cmd, "w"));
        }
    }

}
