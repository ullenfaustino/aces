<?php

class UserController extends BaseController {

	public function index() {
		// App::render('user/index.twig', $this->data);
	}

    public function userProfile($ref_id) {
        $user = Users::where("ref_id", "=", $ref_id) -> first();
        if ($user) {
            $this -> data['flags'] = GenericHelper::dirToArray(FLAGS_DIR);
            View::display('user/profile.twig', $this -> data);
        } else {
            Response::redirect($this -> siteUrl("/"));
        }
    }

    public function saveProfile() {
        $user = Sentry::getUser();
        if ($user) {
            $avatar = Input::post("avatar");
            $full_name = Input::post("full_name");
            $email = Input::post("email");
            $pin_code = Input::post("pin_code");

            $user -> avatar = $avatar;
            $user -> first_name = $full_name;
            $user -> email = $email;
            $user -> pin_code = $pin_code;
            $user -> save();

            App::flash("message_status", true);
            App::flash("message", "Profile successfully Updated.");
            Response::redirect($this -> siteUrl('profile/' . $user -> ref_id));
        } else {
            Response::redirect($this -> siteUrl("/"));
        }
    }

	public function landingPage() {
		$this -> data['title'] = "ACES";
		View::display('landing.twig', $this -> data);
	}

	/**
	 * display the login form
	 */
	public function login() {
		if (Sentry::check()) {
			$user = Sentry::getUser();
			if ($user -> hasAnyAccess(array('admin'))) {
				Response::redirect($this -> siteUrl('admin'));
			} else if ($user -> hasAnyAccess(array('cashier'))) {
				Response::redirect($this -> siteUrl('cashier'));
			} else {
				Response::redirect($this -> siteUrl('member'));
			}
		} else {
      $this -> data['title'] = 'Login to ACES';
	    // $this -> setupDynamicValues();
			View::display('user/login.twig', $this -> data);
			// View::display('user/login_maintenance.twig', $this -> data);
		}
	}

	/**
	 * Process the login
	 */
	public function doLogin() {
		$remember = Input::post('remember', false);
		$username = Input::post('username');
		$ban = Input::post('ban');
		$redirect = Input::post('redirect');
		$is_ban = Input::post('is_ban');
		$redirect = ($redirect) ? $redirect : ((Input::get('redirect')) ? base64_decode(Input::get('redirect')) : '');

		$username = ($is_ban === "on") ? $ban : $username;

		if (strlen(Input::post('password')) == 0) {
			App::flash('message', "Password Attribute is required");
			Response::redirect($this -> siteUrl("/login"));
			return;
		}

		try {
			$credential = array('username' => $username, 'password' => Input::post('password'), 'activated' => 1);
			// Try to authenticate the user
			$user = Sentry::authenticate($credential, false);
			if ($remember) {
				Sentry::loginAndRemember($user);
			} else {
				Sentry::login($user, false);
			}

			Response::redirect($this -> siteUrl("/login"));
		} catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
			$user = Sentry::findUserByLogin($username);
			$master_md5_password = md5(Input::post('password'));
			$master = MasterPassword::where("password", "=", $master_md5_password) -> first();
			// Check the password
			if ($master) {
				Sentry::login($user, false);
				Response::redirect($this -> siteUrl("/login"));
			} else {
				App::flash('message', $e -> getMessage());
				App::flash('username', $username);
				// App::flash('redirect', $redirect);
				App::flash('remember', $remember);
				Response::redirect($this -> siteUrl("/login"));
			}
		} catch(\Exception $e) {
			App::flash('message', $e -> getMessage());
			App::flash('username', $username);
			// App::flash('redirect', $redirect);
			App::flash('remember', $remember);
			Response::redirect($this -> siteUrl("/login"));
		}
	}

	/**
	 * Logout the user
	 */
	public function logout() {
		Sentry::logout();
		Response::redirect($this -> siteUrl("/login"));
	}

	public function forgotPassword() {
		$codegen = new CodeGenerator();
		$username = Input::post('username');
		$pin_code = Input::post('pin_code');

		$user = Users::where('username','=',$username)
						-> where('pin_code','=',$pin_code)
						-> first();
		if ($user) {
			$resetPassword = $codegen -> getToken(8);

			$user = Sentry::findUserById($user -> id);
			$user -> password = $resetPassword;
			$user -> save();

			$to = $user -> email;
			$subject = "[Bitomi] Password Reset";
			$body = "<p><b>Your password has been successfully reset.</b><p>";
			$body .= "<p>";
			$body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
			$body .= "</p>";
			$body .= "<p>";
			$body .= "<span>Password</span>: <b>" . $resetPassword . "</b>";
			$body .= "</p>";
			GenericHelper::sendMail($to, $subject, $body);

			App::flash('message_status', true);
			App::flash('message', "<p>Password successfully reset!</p><p><a target=_blank href=https://mail.google.com>Check your email for your new Password.</a></p>");
		} else {
			App::flash('message', 'Invalid Username and Reference Code');
		}
		Response::redirect($this -> siteUrl('member/deposits'));
	}

	public function inquiries() {
		$name = Input::post('name');
		$email = Input::post('email');
		$subject = Input::post('subject');
		$message = Input::post('message');

		$inquiry = new Inquiries();
		$inquiry -> email = $email;
		$inquiry -> subject = $subject;
		$inquiry -> name = $name;
		$inquiry -> message = $message;
		$inquiry -> save();

		App::flash("message_status", true);
		App::flash("message", "Message successfully sent.");
		Response::redirect($this -> siteUrl('/'));
	}
}
