<?php

namespace Admin;

use \App;
use \View;
use \Input;
use \Sentry;
use \Response;

class AdminController extends BaseController {

	/**
	 * display the admin dashboard
	 */
	public function index() {
		$this->data['user'] = Sentry::getUser();

		Response::Redirect($this -> siteUrl('admin/dashboard'));
	}

}