<?php

/**
 * Sample group routing with user check in middleware
 */
Route::group('/admin', function() {
	if (!Sentry::check()) {
		if (Request::isAjax()) {
			Response::headers() -> set('Content-Type', 'application/json');
			Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
			App::stop();
		} else {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	} else {
		$user = Sentry::getUser();
		if (!$user -> hasAnyAccess(array('admin'))) {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	}
}, function() use ($app) {
	/** sample namespaced controller */
	Route::get('/', 'Admin\AdminController:index') -> name('admin');
	foreach (Module::getModules() as $module) {
		$module -> registerAdminRoute();
	}
});

Route::group('/member', function() {
	if (!Sentry::check()) {
		if (Request::isAjax()) {
			Response::headers() -> set('Content-Type', 'application/json');
			Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
			App::stop();
		} else {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	} else {
		$user = Sentry::getUser();
		if ($user -> hasAnyAccess(array('admin', 'cashier'))) {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	}
}, function() use ($app) {
	/** sample namespaced controller */
	Route::get('/', 'Member\MemberController:index') -> name('member');
	Route::get('/set/password/:id', 'Member\MemberController:setMemberPassword');

	Route::post('/set/new/password', 'Member\MemberController:setMemberNewPassword') -> name("set_new_password");
	Route::post('/set/current/password', 'Member\MemberController:setMemberCurrentPassword') -> name("set_current_password");

	foreach (Module::getModules() as $module) {
        $module -> registerMemberRoute();
    }
});

Route::get('/FBCallbackHandler', 'FacebookHandlerController:FBCallbackHandler');
Route::post('/webhookHandler/:wallet_address', 'APIController:webhookHandler');

/** GET METHOD **/
Route::get('/', 'UserController:landingPage');
Route::get('/profile/:ref_id', 'UserController:userProfile');
Route::get('/login', 'UserController:login') -> name('login');
Route::get('/logout', 'UserController:logout') -> name('logout');
Route::get('/registration', 'RegistrationController:registration_view');
Route::get('/registration/:direct_referral', 'RegistrationController:registration_view_dr');
Route::get('/registration/confirmation/:username/:activation_code', 'RegistrationController:confirmation');

/** POST METHOD **/
Route::post('/login', 'UserController:doLogin') -> name('dologin');
Route::post('/profile/user/save', 'UserController:saveProfile') -> name('save_profile');
Route::post('/pre-registration', 'RegistrationController:pre_registration') -> name('pre_registration');
Route::post('/change/masterpassword', 'Admin\AdminController:changeMasterPassword') -> name('change_master');
Route::post('/change/adminpassword', 'Admin\AdminController:changePassword') -> name('change_admin_password');
Route::post('/toggleregistration', 'Admin\AdminController:toggleRegistration') -> name('toggle_registration');
Route::post('/togglepayout', 'Admin\AdminController:togglePayout') -> name('toggle_payout');
Route::post('/send/new/inquiries', 'UserController:inquiries') -> name('send_inquiries');

// API ROUTES
Route::get('/api/test/mail', 'APIController:testMailer');
Route::get('/api/check_investments_validity', 'APIController:checkInvestmentsValidity');
Route::get('/api/process_investments', 'APIController:processInvestments');
Route::get('/api/convert/btc/:tc_amount', 'APIController:convertToBTC');
Route::get('/api/convert/aud/:tc_amount', 'APIController:convertToAUD');
Route::get('/api/convert/btc_tc/:btc', 'APIController:convertBTC_TC');
Route::get('/api/convert/aud_tc/:aud', 'APIController:convertAUD_TC');

Route::get('/api/convert/buy/btc/:tc_amount', 'APIController:convertToBTC_BUY');
Route::get('/api/convert/buy/aud/:tc_amount', 'APIController:convertToAUD_BUY');
Route::get('/api/convert/buy/btc_tc/:btc', 'APIController:convertBTC_TC_BUY');
Route::get('/api/convert/buy/aud_tc/:aud', 'APIController:convertAUD_TC_BUY');

Route::get('/api/add/dummy_accounts', 'APIController:addDummyAccounts');
Route::get('/api/wallet/details', 'APIController:companyWalletDetails');
