<?php

session_cache_limiter(false);
session_start();

$configINI = parse_ini_file(__DIR__ . "/../config.ini", TRUE);

define('ROOT_PATH'  , __DIR__.'/../../');
define('VENDOR_PATH', __DIR__.'/../../vendor/');
define('APP_PATH'   , __DIR__.'/../../app/');
define('MODULE_PATH', __DIR__.'/../../app/modules/');
define('PUBLIC_PATH', __DIR__.'/../../public/');
define('WORKERS_PATH', __DIR__ . '/../../src/workers');
define('_BASE_URL', $configINI["base_url"]["url"]);

//image upload avatar path
define('AVATAR_PATH', 'assets/images/avatar');
define('FLAGS_DIR', __DIR__ . '/../../public/assets/img/flags');
define('PRODUCT_IMG_PATH', 'assets/images/products');
define('FLAG_PATH', 'assets/img/flags/');
define('SOCKET_PORT', $configINI['socket_port']['port']);
define('BITCOIN_RATES_FILE_PATH', __DIR__ . '/../../src/node_workers/bitcoin_rates.json');
define('SEND_EMAIL', $configINI["notifications"]["send_mail"]);

// GLOBAL DEFINED
define('BLOCKTRAIL_WALLET_USERNAME', $configINI['blocktrail']['wallet_username']);
define('BLOCKTRAIL_WALLET_PASSWORD', $configINI['blocktrail']['wallet_password']);
define('BLOCKTRAIL_COMPANY_WALLET', $configINI['blocktrail']['company_wallet']);
define('BLOCKTRAIL_API_KEY', $configINI['blocktrail']['api_key']);
define('BLOCKTRAIL_API_SECRET', $configINI['blocktrail']['api_secret']);
define('BLOCKTRAIL_IS_TEST_BTC', $configINI['blocktrail']['is_test_btc']);

define('EMAIL_USERNAME', $configINI['email']['username']);
define('EMAIL_PASSWORD', $configINI['email']['password']);

require VENDOR_PATH.'autoload.php';

/**
 * Load the configuration
 */
$config = array(
    'path.root'     => ROOT_PATH,
    'path.public'   => PUBLIC_PATH,
    'path.app'      => APP_PATH,
    'path.module'   => MODULE_PATH
);

foreach (glob(APP_PATH.'config/*.php') as $configFile) {
    require $configFile;
}

/** Merge cookies config to slim config */
if(isset($config['cookies'])){
    foreach($config['cookies'] as $configKey => $configVal){
        $config['slim']['cookies.'.$configKey] = $configVal;
    }
}

/**
 * Initialize Slim and SlimStarter application
 */
$app        = new \Slim\Slim($config['slim']);
$starter    = new \SlimStarter\Bootstrap($app);

$starter->setConfig($config);

/**
 * if called from the install script, disable all hooks, middlewares, and database init
 */
if(!defined('INSTALL')){
    /** boot up SlimStarter */
    $starter->boot();

    /** Setting up Slim hooks and middleware */
    require APP_PATH.'bootstrap/app.php';

    /** registering modules */
    foreach (glob(APP_PATH.'modules/*') as $module) {
        $className = basename($module);
        $moduleBootstrap = "\\$className\\Initialize";

        $app->module->register(new $moduleBootstrap);
    }

    $app->module->boot();

    /** Start the route */
    require APP_PATH.'routes.php';
}else{
    /** disregard sentry configuration on install */
    $config['aliases']['Sentry'] = 'Cartalyst\Sentry\Facades\Native\Sentry';

    $starter->bootFacade($config['aliases']);
}

return $starter;
