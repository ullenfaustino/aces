<?php

/**
 * Hook, filter, etc should goes here
 */

$timezone = "Australia/Brisbane"; // sets the timezone or region
if(function_exists('date_default_timezone_set')){ // the if function checks if setting the timezone is supported on your server first. You don't want an error thrown to the user do you..?
    date_default_timezone_set($timezone);
}

/**
 * error handling sample
 *
 * $app->error(function() use ($app){
 *     $app->render('error.html');
 * });
 */
