<?php

namespace AdminSettings\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \GeneralSettings;
use \MasterPassword;
use \ChangeLogs;

class AdminSettingsController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data["active_menu"] = "admin_settings";
    }

    public function index() {
        $user = Sentry::getUser();

        $this -> data['title'] = 'Settings';
        $this -> data['user'] = $user;

        View::display('@adminsettings/index.twig', $this -> data);
    }

    public function saveSellValue() {
      $sell_value = Input::post("sell_value");
      $settings = GeneralSettings::where("module_name","=","sell_value") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "sell_value";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $sell_value;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "sell_value";
      $logs -> current_value = $current_value;
      $logs -> new_value = $sell_value;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function saveBuyValue() {
      $buy_value = Input::post("buy_value");
      $settings = GeneralSettings::where("module_name","=","buy_value") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "buy_value";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $buy_value;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "buy_value";
      $logs -> current_value = $current_value;
      $logs -> new_value = $buy_value;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function saveSellValueBTC() {
      $sell_value_btc = Input::post("sell_value_btc");
      $settings = GeneralSettings::where("module_name","=","sell_value_btc") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "sell_value_btc";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $sell_value_btc;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "sell_value_btc";
      $logs -> current_value = $current_value;
      $logs -> new_value = $sell_value_btc;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function saveBuyValueBTC() {
      $buy_value_btc = Input::post("buy_value_btc");
      $settings = GeneralSettings::where("module_name","=","buy_value_btc") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "buy_value_btc";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $buy_value_btc;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "buy_value_btc";
      $logs -> current_value = $current_value;
      $logs -> new_value = $buy_value_btc;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function savePackageRange_1() {
      $package_1_range = Input::post("package_1_range");
      $settings = GeneralSettings::where("module_name","=","package_1_range") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "package_1_range";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $package_1_range;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "package_1_range";
      $logs -> current_value = $current_value;
      $logs -> new_value = $package_1_range;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function savePackageRange_2() {
      $package_2_range = Input::post("package_2_range");
      $settings = GeneralSettings::where("module_name","=","package_2_range") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "package_2_range";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $package_2_range;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "package_2_range";
      $logs -> current_value = $current_value;
      $logs -> new_value = $package_2_range;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function savePackageInterest_1() {
      $package_1_interest = Input::post("package_1_interest");
      $settings = GeneralSettings::where("module_name","=","package_1_interest") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "package_1_interest";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $package_1_interest;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "package_1_interest";
      $logs -> current_value = $current_value;
      $logs -> new_value = $package_1_interest;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function savePackageInterest_2() {
      $package_2_interest = Input::post("package_2_interest");
      $settings = GeneralSettings::where("module_name","=","package_2_interest") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "package_2_interest";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $package_2_interest;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "package_2_interest";
      $logs -> current_value = $current_value;
      $logs -> new_value = $package_2_interest;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function savePackageLockin_1() {
      $package_1_lockin = Input::post("package_1_lockin");
      $settings = GeneralSettings::where("module_name","=","package_1_lockin") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "package_1_lockin";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $package_1_lockin;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "package_1_lockin";
      $logs -> current_value = $current_value;
      $logs -> new_value = $package_1_lockin;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function savePackageLockin_2() {
      $package_2_lockin = Input::post("package_2_lockin");
      $settings = GeneralSettings::where("module_name","=","package_2_lockin") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "package_2_lockin";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $package_2_lockin;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "package_2_lockin";
      $logs -> current_value = $current_value;
      $logs -> new_value = $package_2_lockin;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function saveDrBonus() {
      $dr_bonus = Input::post("dr_bonus");
      $settings = GeneralSettings::where("module_name","=","dr_bonus") -> first();
      if (!$settings) {
        $settings = new GeneralSettings();
        $settings -> module_name = "dr_bonus";
      }
      $current_value = ($settings) ? $settings -> content : 0;
      $settings -> content = $dr_bonus;
      $settings -> save();

      $logs = new ChangeLogs();
      $logs -> module_name = "dr_bonus";
      $logs -> current_value = $current_value;
      $logs -> new_value = $dr_bonus;
      $logs -> updated_by = Sentry::getUser() -> id;
      $logs -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function setSwitcher() {
      $registration_switch = Input::post("registration_switch");
      $payout_switch = Input::post("payout_switch");

      $registration_status = ($registration_switch === "on") ? 1 : 0;
      $payout_status = ($payout_switch === "on") ? 1 : 0;

      // Registration
      $registration = GeneralSettings::where("module_name","=","registration") -> first();
      if (!$registration) {
        $registration = new GeneralSettings();
        $registration -> module_name = "registration";
      }
      $registration -> content = $registration_status;
      $registration -> save();

      // Payout
      $payout = GeneralSettings::where("module_name","=","payout") -> first();
      if (!$payout) {
        $payout = new GeneralSettings();
        $payout -> module_name = "payout";
      }
      $payout -> content = $payout_status;
      $payout -> save();

      App::flash("message_status", true);
      App::flash("message","Successfully Saved.");
      Response::redirect($this -> siteUrl("admin/settings"));
    }

    public function changeMasterPassword() {
      $master_password = trim(Input::post("master_password"));

      if (strlen($master_password) == 0) {
        App::flash("message_status", false);
        App::flash("message","Unable to change Master password.");
        Response::redirect($this -> siteUrl("admin/settings"));
        return;
      }

      $master = MasterPassword::where("is_active","=",1) -> first();
      if ($master) {
        $master -> password = md5($master_password);
        $master -> save();

        App::flash("message_status", true);
        App::flash("message","Successfully Saved.");
      } else {
        App::flash("message_status", false);
        App::flash("message","Unable to change Master password.");
      }
      Response::redirect($this -> siteUrl("admin/settings"));
    }

}
