<?php

namespace AdminSettings;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'AdminSettings';
	}
	public function getModuleAccessor() {
		return 'adminsettings';
	}
	public function registerAdminRoute() {
		Route::resource ( '/settings', 'AdminSettings\Controllers\AdminSettingsController' );

		Route::post ( '/settings/save/sell_value', 'AdminSettings\Controllers\AdminSettingsController:saveSellValue' ) -> name("save_sell_value");
		Route::post ( '/settings/save/buy_value', 'AdminSettings\Controllers\AdminSettingsController:saveBuyValue' ) -> name("save_buy_value");
		Route::post ( '/settings/save/sell_value_btc', 'AdminSettings\Controllers\AdminSettingsController:saveSellValueBTC' ) -> name("save_sell_value_btc");
		Route::post ( '/settings/save/buy_value_btc', 'AdminSettings\Controllers\AdminSettingsController:saveBuyValueBTC' ) -> name("save_buy_value_btc");
		Route::post ( '/settings/save/package_1_range', 'AdminSettings\Controllers\AdminSettingsController:savePackageRange_1' ) -> name("save_package_1_range");
		Route::post ( '/settings/save/package_2_range', 'AdminSettings\Controllers\AdminSettingsController:savePackageRange_2' ) -> name("save_package_2_range");
		Route::post ( '/settings/save/package_1_interest', 'AdminSettings\Controllers\AdminSettingsController:savePackageInterest_1' ) -> name("save_package_1_interest");
		Route::post ( '/settings/save/package_2_interest', 'AdminSettings\Controllers\AdminSettingsController:savePackageInterest_2' ) -> name("save_package_2_interest");
		Route::post ( '/settings/save/package_1_lockin', 'AdminSettings\Controllers\AdminSettingsController:savePackageLockin_1' ) -> name("save_package_1_lockin");
		Route::post ( '/settings/save/package_2_lockin', 'AdminSettings\Controllers\AdminSettingsController:savePackageLockin_2' ) -> name("save_package_2_lockin");
		Route::post ( '/settings/save/dr_bonus', 'AdminSettings\Controllers\AdminSettingsController:saveDrBonus' ) -> name("save_dr_bonus");
		Route::post ( '/settings/save/switcher', 'AdminSettings\Controllers\AdminSettingsController:setSwitcher' ) -> name("set_switcher");
		Route::post ( '/settings/save/master_password', 'AdminSettings\Controllers\AdminSettingsController:changeMasterPassword' ) -> name("set_master_password");
	}
}
