<?php

namespace AdminLandingPage\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \GeneralSettings;

class AdminLandingPageController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data["active_menu"] = "admin_landing_page";
    }

    public function index() {
        $user = Sentry::getUser();

        $this -> data['title'] = 'Landing Page Settings';
        $this -> data['user'] = $user;

        View::display('@adminlandingpage/index.twig', $this -> data);
    }

    public function saveDescription() {
      $description = Input::post("description");
      $landing_settings = GeneralSettings::where('module_name','=','landing_description') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_description";
      }
      $landing_settings -> content = $description;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function saveAbout() {
      $about = Input::post("about");
      $landing_settings = GeneralSettings::where('module_name','=','landing_about') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_about";
      }
      $landing_settings -> content = $about;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function saveMission() {
      $mission = Input::post("mission");
      $landing_settings = GeneralSettings::where('module_name','=','landing_mission') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_mission";
      }
      $landing_settings -> content = $mission;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function saveVision() {
      $vision = Input::post("vision");
      $landing_settings = GeneralSettings::where('module_name','=','landing_vision') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_vision";
      }
      $landing_settings -> content = $vision;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function saveAddress() {
      $address = Input::post("formatted_address");
      $latitude = Input::post("latitude");
      $longitude = Input::post("longitude");

      $landing_settings = GeneralSettings::where('module_name','=','landing_address') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_address";
      }
      $landing_settings -> content = $address;
      $landing_settings -> save();

      $landing_settings = GeneralSettings::where('module_name','=','landing_lat') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_lat";
      }
      $landing_settings -> content = $latitude;
      $landing_settings -> save();

      $landing_settings = GeneralSettings::where('module_name','=','landing_lng') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_lng";
      }
      $landing_settings -> content = $longitude;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function savePhone() {
      $phone = Input::post("phone");
      $landing_settings = GeneralSettings::where('module_name','=','landing_phone') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_phone";
      }
      $landing_settings -> content = $phone;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function saveEmail() {
      $email = Input::post("email");
      $landing_settings = GeneralSettings::where('module_name','=','landing_email') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_email";
      }
      $landing_settings -> content = $email;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function saveFacebook() {
      $facebook = Input::post("facebook");
      $landing_settings = GeneralSettings::where('module_name','=','landing_facebook') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_facebook";
      }
      $landing_settings -> content = $facebook;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function saveTwitter() {
      $twitter = Input::post("twitter");
      $landing_settings = GeneralSettings::where('module_name','=','landing_twitter') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_twitter";
      }
      $landing_settings -> content = $twitter;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }

    public function saveGoogle() {
      $google = Input::post("google");
      $landing_settings = GeneralSettings::where('module_name','=','landing_google') -> first();
      if (!$landing_settings) {
        $landing_settings = new GeneralSettings();
        $landing_settings -> module_name = "landing_google";
      }
      $landing_settings -> content = $google;
      $landing_settings -> save();

      App::flash("message_status", true);
      App::flash("message", "Settings successfully saved.");
      Response::redirect($this -> siteUrl('admin/landingpage'));
    }
}
