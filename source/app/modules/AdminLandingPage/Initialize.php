<?php

namespace AdminLandingPage;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'AdminLandingPage';
	}
	public function getModuleAccessor() {
		return 'adminlandingpage';
	}
	public function registerAdminRoute() {
		Route::resource ( '/landingpage', 'AdminLandingPage\Controllers\AdminLandingPageController' );

		Route::post ( '/landingpage/description', 'AdminLandingPage\Controllers\AdminLandingPageController:saveDescription' ) -> name('landing_description');
		Route::post ( '/landingpage/about', 'AdminLandingPage\Controllers\AdminLandingPageController:saveAbout' ) -> name('landing_about');
		Route::post ( '/landingpage/mission', 'AdminLandingPage\Controllers\AdminLandingPageController:saveMission' ) -> name('landing_mission');
		Route::post ( '/landingpage/vision', 'AdminLandingPage\Controllers\AdminLandingPageController:saveVision' ) -> name('landing_vision');
		Route::post ( '/landingpage/address', 'AdminLandingPage\Controllers\AdminLandingPageController:saveAddress' ) -> name('landing_address');
		Route::post ( '/landingpage/phone', 'AdminLandingPage\Controllers\AdminLandingPageController:savePhone' ) -> name('landing_phone');
		Route::post ( '/landingpage/email', 'AdminLandingPage\Controllers\AdminLandingPageController:saveEmail' ) -> name('landing_email');
		Route::post ( '/landingpage/facebook', 'AdminLandingPage\Controllers\AdminLandingPageController:saveFacebook' ) -> name('landing_facebook');
		Route::post ( '/landingpage/twitter', 'AdminLandingPage\Controllers\AdminLandingPageController:saveTwitter' ) -> name('landing_twitter');
		Route::post ( '/landingpage/google', 'AdminLandingPage\Controllers\AdminLandingPageController:saveGoogle' ) -> name('landing_google');
	}
}
