<?php

namespace AdminInquiries\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \Inquiries;

class AdminInquiriesController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data["active_menu"] = "admin_inquries";
    }

    public function index() {
        $user = Sentry::getUser();

        $this -> data['title'] = 'Inquiries';
        $this -> data['user'] = $user;

        $this -> data['inquiries'] = Inquiries::all();

        View::display('@admininquiries/index.twig', $this -> data);
    }

    public function setAsResponded() {
      $inquiry_id = Input::post("inquiry_id");

      $inquiry = Inquiries::find($inquiry_id);
      $inquiry -> is_responded = 1;
      $inquiry -> save();

      App::flash("message_status", true);
  		App::flash("message", "Inquiry successfully responded.");
  		Response::redirect($this -> siteUrl('admin/inquiries'));
    }

}
