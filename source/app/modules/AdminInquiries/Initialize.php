<?php

namespace AdminInquiries;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
    public function getModuleName() {
        return 'AdminInquiries';
    }

    public function getModuleAccessor() {
        return 'admininquiries';
    }

    public function registerAdminRoute() {
        Route::resource('/inquiries', 'AdminInquiries\Controllers\AdminInquiriesController');

        Route::post('/inquiries/set/responded', 'AdminInquiries\Controllers\AdminInquiriesController:setAsResponded') -> name("set_as_responded");
    }

}
