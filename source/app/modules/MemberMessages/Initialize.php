<?php

namespace MemberMessages;

use \App;
use \Menu;
use \Route;
use \Sentry;
use \Messages;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'MemberMessages';
	}
	public function getModuleAccessor() {
		return 'membermessages';
	}
	public function registerMemberRoute() {
		Route::resource ( '/messages', 'MemberMessages\Controllers\MemberMessagesController' );

		Route::post('/send/message', 'MemberMessages\Controllers\MemberMessagesController:sendMessage') -> name('send_message');

		Route::get('/thread/replies/:msg_ref_id', 'MemberMessages\Controllers\MemberMessagesController:msgrepliesview');
	}
}
