<?php

namespace AdminInvestments;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'AdminInvestments';
	}
	public function getModuleAccessor() {
		return 'admininvestments';
	}
	public function registerAdminRoute() {
		Route::resource ( '/investments', 'AdminInvestments\Controllers\AdminInvestmentsController' );

		Route::get ( '/investments/interest_logs/:trans_code', 'AdminInvestments\Controllers\AdminInvestmentsController:interestLogsView' );
	}
}
