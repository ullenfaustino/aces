<?php

namespace AdminInvestments\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \Payouts;
use \InvestmentLogs;
use \InterestLogs;
use \PurchaseLogs;

class AdminInvestmentsController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data["active_menu"] = "admin_investments";
    }

    public function index() {
        $user = Sentry::getUser();

        $this -> data['title'] = 'Member Investments';
        $this -> data['user'] = $user;

        $investments = InvestmentLogs::all();
        $this -> data["investments"] = $investments;

        $this -> data["purchase_logs"] = PurchaseLogs::all();

        View::display('@admininvestments/index.twig', $this -> data);
    }

    public function interestLogsView($trans_code) {
        $user = Sentry::getUser();
        $this -> data['user'] = $user;

        $this -> data['title'] = 'Investments interest logs';
        $this -> data['trans_code'] = $trans_code;

        $this -> data['interest_logs'] = InterestLogs::where("investment_code", "=", $trans_code) -> get();

        View::display('@admininvestments/interest_logs.twig', $this -> data);
    }
}
