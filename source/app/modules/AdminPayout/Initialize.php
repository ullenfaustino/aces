<?php

namespace AdminPayout;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'AdminPayout';
	}
	public function getModuleAccessor() {
		return 'adminpayout';
	}
	public function registerAdminRoute() {
		Route::resource ( '/payout', 'AdminPayout\Controllers\AdminPayoutController' );

		Route::post ( '/payout/approve', 'AdminPayout\Controllers\AdminPayoutController:approveRequest' ) -> name("admin_approve_request");
	}
}
