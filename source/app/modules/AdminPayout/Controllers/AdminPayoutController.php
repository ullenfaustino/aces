<?php

namespace AdminPayout\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \Payouts;
use \InvestmentLogs;
use \InterestLogs;

class AdminPayoutController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data["active_menu"] = "admin_payout";
    }

    public function index() {
        $user = Sentry::getUser();

        $this -> data['title'] = 'Payout';
        $this -> data['user'] = $user;

        $this -> data["payouts"] = Payouts::where("status", "=", 0) -> get();
        $this -> data["approved_payouts"] = Payouts::where("status", "=", 1) -> get();

        View::display('@adminpayout/index.twig', $this -> data);
    }

    public function approveRequest() {
      $reference_code = Input::post("reference_code");

      $payout = Payouts::where("ref_code", "=", $reference_code) -> first();

      if ($payout) {
        if ($payout -> request_type == 1) {
          $hhh = InterestLogs::find($payout->link_id);
        } else {
          $hhh = InvestmentLogs::find($payout->link_id);
        }
        $hhh -> is_withdrawed = 1;
        $hhh -> save();

        $payout -> status = 1;
        $payout -> approved_by = Sentry::getUser() -> id;
        $payout -> save();

        App::flash("message_status", true);
        App::flash("message", "Payout request successfully approved.");
      } else {
        App::flash("message_status", false);
        App::flash("message", "Unable to process payout request.");
      }
      Response::redirect($this -> siteUrl("admin/payout"));
    }

}
