<?php

namespace AAMemberTransactions;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'AAMemberTransactions';
	}
	public function getModuleAccessor() {
		return 'membertransactions';
	}
	public function registerMemberRoute() {
		Route::resource ( '/transaction', 'AAMemberTransactions\Controllers\MemberTransactionsController' );

		Route::get ( '/transaction/view/details/:trans_code', 'AAMemberTransactions\Controllers\MemberTransactionsController:viewInvestmentDetails' );

		Route::post ( '/transaction/add/investment', 'AAMemberTransactions\Controllers\MemberTransactionsController:addInvestment' ) -> name('add_investment');
		Route::post ( '/transaction/payout/interest', 'AAMemberTransactions\Controllers\MemberTransactionsController:requestInterestPayout' ) -> name('payout_interest');
		Route::post ( '/transaction/payout/investment', 'AAMemberTransactions\Controllers\MemberTransactionsController:requestInvestmentPayout' ) -> name('payout_investment');
		Route::post ( '/transaction/payout/withdrawal', 'AAMemberTransactions\Controllers\MemberTransactionsController:requestWithdrawal' ) -> name('bitcoin_withdrawal');
	}
}
