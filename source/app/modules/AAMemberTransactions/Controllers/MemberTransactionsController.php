<?php

namespace AAMemberTransactions\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \InvestmentLogs;
use \InterestLogs;
use \CodeGenerator;
use \Payouts;
use \GenericHelper;
use \ConvertionLogs;

class MemberTransactionsController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data['active_menu'] = 'member_transactions';
    }

    public function index() {
        $user = Sentry::getUser();
        $this -> data['title'] = 'Transactions';

        $investments = InvestmentLogs::where("user_id", "=", $user -> id) -> get();
        $this -> data["investments"] = $investments;

        $withdrawals = ConvertionLogs::where("user_id", "=", $user -> id) -> get();
        $this -> data["withdrawals"] = $withdrawals;

        View::display('@membertransactions/index.twig', $this -> data);
    }

    public function viewInvestmentDetails($trans_code) {
        $this -> data['title'] = 'Investment Interest Logs';

        $interest_logs = InterestLogs::where("investment_code", "=", $trans_code) -> get();
        $this -> data["interest_logs"] = $interest_logs;
        $this -> data["trans_code"] = $trans_code;

        View::display('@membertransactions/details.twig', $this -> data);
    }

    public function addInvestment() {
        $user = Sentry::getUser();
        $request_uri = Input::post("request_uri");
        $investment_amount = Input::post("investment_amount");
        $package_type = Input::post("package_type");

        $remainingBalance = (double)GenericHelper::availableCoinBalance($user -> id) - (double)$investment_amount;

        if ((strlen($investment_amount) == 0) || ($investment_amount == 0)) {
            App::flash("message_status", false);
            App::flash("message", "Invalid AC Amount.");
            Response::redirect($this -> siteUrl($request_uri));
            return;
        }

        if ((double)GenericHelper::availableCoinBalance($user -> id) <= 1) {
            App::flash("message_status", false);
            App::flash("message", "You must have 1AC remaining in your balance.");
            Response::redirect($this -> siteUrl($request_uri));
            return;
        }

        if ($remainingBalance < 1) {
            App::flash("message_status", false);
            App::flash("message", "You must have 1AC remaining in your balance.");
            Response::redirect($this -> siteUrl($request_uri));
            return;
        }

        if ((double)$investment_amount > (double)GenericHelper::availableCoinBalance($user -> id)) {
            App::flash("message_status", false);
            App::flash("message", "Insufficient AC Amount.");
            Response::redirect($this -> siteUrl($request_uri));
            return;
        }

        $codeGen = new CodeGenerator();

        $investment = new InvestmentLogs();
        $investment -> trans_code = $codeGen -> getToken(8);
        $investment -> user_id = $user -> id;
        $investment -> tc_amount = $investment_amount;
        $investment -> package_type = $package_type;
        $investment -> days_range = ($package_type == 1) ? $this -> data['package_1_range'] : $this -> data['package_2_range'];
        $investment -> interest = ($package_type == 1) ? $this -> data['package_1_interest'] : $this -> data['package_2_interest'];
        $investment -> lock_in = ($package_type == 1) ? $this -> data['package_1_lockin'] : $this -> data['package_2_lockin'];
        $investment -> save();

        if ($user -> has_investment == 0 && $investment_amount >= 1) {
            $user -> has_investment = 1;
            $user -> save();
        }

        App::flash("message_status", true);
        App::flash("message", "Investment successfully saved.");
        Response::redirect($this -> siteUrl($request_uri));
    }

    public function requestInterestPayout() {
        $codeGen = new CodeGenerator();
        $interest_id = Input::post("interest_id");
        // $request_uri = Input::post("request_uri");
        $interest = InterestLogs::find($interest_id);

        $payout_request = Payouts::where("link_id", "=", $interest -> id) -> where("request_type", "=", 1) -> where("status", "=", 0) -> first();
        if (!$payout_request) {
            $payout_request = new Payouts();
            $payout_request -> ref_code = $codeGen -> getToken(8);
            $payout_request -> user_id = Sentry::getUser() -> id;
            $payout_request -> request_type = 1;
            $payout_request -> link_id = $interest -> id;
            $payout_request -> tc_amount = $interest -> interest_amount;
            $payout_request -> save();

            App::flash("message_status", true);
            App::flash("message", "Request successfully sent to the Admin.");
        } else {
            App::flash("message_status", false);
            App::flash("message", "Unable to submit payout request, You have pending payout request.");
        }
        Response::redirect($this -> siteUrl("member/transaction/view/details/" . $interest -> investment_code));
    }

    public function requestInvestmentPayout() {
        $codeGen = new CodeGenerator();
        $investment_id = Input::post("investment_id");
        // $request_uri = Input::post("request_uri");
        $investment = InvestmentLogs::find($investment_id);

        $payout_request = Payouts::where("link_id", "=", $investment -> id) -> where("request_type", "=", 2) -> where("status", "=", 0) -> first();
        if (!$payout_request) {
            $payout_request = new Payouts();
            $payout_request -> ref_code = $codeGen -> getToken(8);
            $payout_request -> user_id = Sentry::getUser() -> id;
            $payout_request -> request_type = 2;
            $payout_request -> link_id = $investment -> id;
            $payout_request -> tc_amount = $investment -> tc_amount;
            $payout_request -> save();

            App::flash("message_status", true);
            App::flash("message", "Request successfully sent to the Admin.");
        } else {
            App::flash("message_status", false);
            App::flash("message", "Unable to submit payout request, You have pending payout request.");
        }
        Response::redirect($this -> siteUrl("member/transaction"));
    }

    public function requestWithdrawal() {
        $user = Sentry::getUser();
        $to_wallet = Input::post("to_wallet");
        $pin_code = Input::post("pin_code");
        $tc_amount = Input::post("tc_amount");
        // $request_uri = Input::post("request_uri");

        if ($pin_code != $user -> pin_code) {
            App::flash("message_status", false);
            App::flash("message", "Invalid Pin Code");
            Response::redirect($this -> siteUrl("member/transaction"));
            return;
        }

        if ($tc_amount < 3) {
            App::flash("message_status", false);
            App::flash("message", "Minimum of 3 AC upon withdrawal");
            Response::redirect($this -> siteUrl("member/transaction"));
            return;
        }

        $btc_amount = GenericHelper::convertTC_BTC_SELL($tc_amount);
        $aud_amount = GenericHelper::convertTC_AUD_SELL($tc_amount);
        $current_amount = GenericHelper::availableCoinBalance($user -> id);

        if ($tc_amount > $current_amount) {
            App::flash("message_status", false);
            App::flash("message", "Insufficient Amount!");
            Response::redirect($this -> siteUrl("member/transaction"));
            return;
        }

        $convertLogs = new ConvertionLogs();
        $convertLogs -> user_id = $user -> id;
        $convertLogs -> current_amount = $current_amount;
        $convertLogs -> withdrawed_amount = $tc_amount;
        $convertLogs -> btc_amount = $btc_amount;
        $convertLogs -> aud_amount = $aud_amount;
        $convertLogs -> to_wallet = $to_wallet;
        try {
            GenericHelper::sendPaymentBTC(BLOCKTRAIL_WALLET_USERNAME, BLOCKTRAIL_WALLET_PASSWORD, $btc_amount, $to_wallet);

            $convertLogs -> save();

            App::flash("message_status", true);
            App::flash("message", "AC successfully sent.");
        } catch(\Exception $e) {App::flash("message_status", false);
            App::flash("message", $e -> getMessage());
        }
        Response::redirect($this -> siteUrl("member/transaction"));
    }

}
