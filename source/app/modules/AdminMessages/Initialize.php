<?php

namespace AdminMessages;

use \App;
use \Menu;
use \Route;
use \Sentry;
use \Messages;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'AdminMessages';
	}
	public function getModuleAccessor() {
		return 'adminmessages';
	}
	public function registerAdminRoute() {
		Route::resource ( '/messages', 'AdminMessages\Controllers\AdminMessagesController' );

		Route::post('/send/message', 'AdminMessages\Controllers\AdminMessagesController:sendMessage') -> name('admin_send_message');

		Route::get('/thread/replies/:msg_ref_id', 'AdminMessages\Controllers\AdminMessagesController:msgrepliesview');
	}
}
