<?php

namespace AdminMessages\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \CodeGenerator;
use \Messages;
use \MessageReplies;

class AdminMessagesController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data["active_menu"] = "admin_messages";
	}

	/**
	 * display list of resource
	 */
	public function index($page = 1) {
		$user = Sentry::getUser();

		$this -> data['title'] = 'Messages';
		$this -> data['user'] = $user;

		$messages = $this -> getMessages($user);
		// print_r(json_encode($messages));
		// exit();
		$this -> data['messages'] = $messages;

		View::display('@adminmessages/index.twig', $this -> data);
	}

	public function msgrepliesview($msg_ref_id) {
		$user = Sentry::getUser();

		$this -> data['title'] = $msg_ref_id;
		$this -> data['user'] = $user;
		$this -> data['message_ref_id'] = $msg_ref_id;

		$msg = Messages::where('ref_id','=',$msg_ref_id) -> first();
		$this -> data['msg'] = $msg;

		if ($msg -> last_sender_user_id != $user -> id) {
			$msg -> is_read = 1;
			$msg -> save();
		}

		$messages = MessageReplies::where('msg_ref_id', '=', $msg_ref_id)
								-> orderBy("created_at","asc")
								-> get();
		$this -> data['messages'] = $messages;

		if ($msg -> last_sender_user_id != $user -> id) {
			foreach ($messages as $key => $message) {
				$message -> is_read = 1;
				$message -> save();
			}
		}

		View::display('@adminmessages/message_thread.twig', $this -> data);
	}

	private function getMessages($user) {
		$messages = Messages::where('to_user_id', '=', $user -> id)
		                  -> orWhere('from_user_id', '=', $user -> id)
		                  -> orderBy('updated_at', 'asc') -> get();

		foreach ($messages as $keyINdex => $message) {
			$conversations = MessageReplies::where('msg_ref_id', '=', $message -> ref_id)
			                             -> orderBy("created_at","asc") -> get();
			foreach ($conversations as $key => $conversation) {
				$lastMessage = $conversation -> reply_body;
			}
			$messages[$keyINdex]["last_message"] = $lastMessage;
			$messages[$keyINdex]["message_body"] = $conversations;
		}
		return $messages;
	}

	public function sendMessage() {
		$user = Sentry::getUser();

		$uri = Input::post("request_uri");
		$sendto = Input::post('send_to');
		$msgBody = Input::post('message_content');
		$msgCode = Input::post('message_ref_code');

        if (($user -> username === $sendto) || ($user -> ref_id === $sendto)) {
            App::flash("message_status", false);
            App::flash("message", "You cannot send message to yourself!");
            Response::redirect($this -> siteUrl($uri));
            return;
        }

		$send_To = Users::where('username', '=', $sendto)
		              -> orWhere('ref_id', '=', $sendto) -> first();
		if (!$send_To) {
			App::flash("message_status", false);
			App::flash("message", "admin not Found.");
			Response::redirect($this -> siteUrl($uri));
			return;
		}

		if (strlen($msgBody) == 0) {
			App::flash("message_status", false);
			App::flash("message", "Message body is empty.");
			Response::redirect($this -> siteUrl($uri));
			return;
		}

		$msg = Messages::where('ref_id', '=', $msgCode) -> first();
		if (!$msg) {
			$codeGen = new CodeGenerator();

			$msg = new Messages();
			$msg -> ref_id = $codeGen -> getToken(10);
		}
		$msg -> to_user_id = $send_To -> id;
		$msg -> from_user_id = $user -> id;
		$msg -> last_sender_user_id = $user -> id;
		$msg -> is_read = 0;
		$msg -> updated_at = date('Y-m-d h:m:s');

		if ($msg -> save()) {
			$conversations = new MessageReplies();
			$conversations -> msg_ref_id = $msg -> ref_id;
			$conversations -> msg_from_user_id = $user -> id;
			$conversations -> reply_body = $msgBody;
			$conversations -> is_read = 0;
			$conversations -> save();
		}

		App::flash("message_status", true);
		App::flash("message", "Message successfully sent.");
		Response::redirect($this -> siteUrl($uri));
	}

}
