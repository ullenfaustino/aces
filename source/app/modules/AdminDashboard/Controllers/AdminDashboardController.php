<?php

namespace AdminDashboard\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \GenericHelper;
use \InvestmentLogs;
use \Payouts;
use \AdminWithdrawals;

class AdminDashboardController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data["active_menu"] = "admin_dashboard";
    }

    public function index() {
        $user = Sentry::getUser();

        $this -> data['title'] = 'Dashboard';
        $this -> data['user'] = $user;
        $this -> data['members'] = Users::where("user_type", "=", 3) -> where("ref_id", "<>", "e98aldgl") -> get();

        $this -> data['investments'] = InvestmentLogs::all();

        $this -> data['total_pending_payout'] = Payouts::where("status","=",0) -> sum('tc_amount');
        $this -> data['total_approved_payout'] = Payouts::where("status","=",1) -> sum('tc_amount');

        View::display('@admindashboard/index.twig', $this -> data);
    }

    public function resendMailConfirmation() {
        $member_id = Input::post("member_id");
        $member = Users::find($member_id);

        GenericHelper::sendAccountVerification($member, $member -> activation_code, "master");

        App::flash('message_status', true);
        App::flash('message', 'Confirmation successfully sent to email.');
        Response::redirect($this -> siteUrl('admin/dashboard'));
    }

    public function transferBitcoin() {
      $external_wallet = Input::post("external_wallet");
      $withdrawable_amount = Input::post("withdrawable_amount");

      $logs = new AdminWithdrawals();
      $logs -> user_id = Sentry::getUser() -> id;
      $logs -> to_wallet = $external_wallet;
      $logs -> bitcoin_amount = $withdrawable_amount;

      try {
        GenericHelper::sendPaymentBTC(BLOCKTRAIL_WALLET_USERNAME, BLOCKTRAIL_WALLET_PASSWORD, $withdrawable_amount, $external_wallet);

        $logs -> save();

        App::flash("message_status", true);
        App::flash("message", "Transaction successfully sent.");
      } catch(\Exception $e) {App::flash("message_status", false);
        App::flash("message", $e -> getMessage());
      }
      Response::redirect($this -> siteUrl("admin/dashboard"));
    }

}
