<?php

namespace AdminDashboard;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
    public function getModuleName() {
        return 'AdminDashboard';
    }

    public function getModuleAccessor() {
        return 'admindashboard';
    }

    public function registerAdminRoute() {
        Route::resource('/dashboard', 'AdminDashboard\Controllers\AdminDashboardController');

        Route::post('/dashboard/mail/confirmation/resend', 'AdminDashboard\Controllers\AdminDashboardController:resendMailConfirmation') -> name("resend_confirmation");
        Route::post('/dashboard/bitcoin/transfer', 'AdminDashboard\Controllers\AdminDashboardController:transferBitcoin') -> name("admin_withdrawal_btc");
    }

}
