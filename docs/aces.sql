-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 22, 2017 at 05:42 PM
-- Server version: 5.7.18
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aces`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_withdrawals`
--

CREATE TABLE `admin_withdrawals` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `to_wallet` varchar(250) DEFAULT NULL,
  `bitcoin_amount` decimal(10,8) DEFAULT '0.00000000',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `change_logs`
--

CREATE TABLE `change_logs` (
  `id` int(11) NOT NULL,
  `module_name` varchar(250) DEFAULT NULL,
  `current_value` varchar(500) DEFAULT NULL,
  `new_value` varchar(500) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `change_logs`
--

INSERT INTO `change_logs` (`id`, `module_name`, `current_value`, `new_value`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'dr_bonus', '1500', '500', 1, '2017-08-22 10:26:34', '2017-08-22 10:26:34');

-- --------------------------------------------------------

--
-- Table structure for table `company_allocated_income`
--

CREATE TABLE `company_allocated_income` (
  `id` int(11) NOT NULL,
  `investment_amount` decimal(10,2) DEFAULT '0.00',
  `user_id` int(11) DEFAULT '0',
  `company_profit` decimal(10,2) DEFAULT '0.00',
  `direct_referral` decimal(10,2) DEFAULT '0.00',
  `opex` decimal(10,2) DEFAULT '0.00',
  `misc` decimal(10,2) DEFAULT '0.00',
  `unilevel` decimal(10,2) DEFAULT '0.00',
  `reserved_funds` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `convertion_logs`
--

CREATE TABLE `convertion_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `current_amount` decimal(10,8) DEFAULT '0.00000000',
  `withdrawed_amount` decimal(10,8) DEFAULT '0.00000000',
  `btc_amount` decimal(10,8) DEFAULT '0.00000000',
  `aud_amount` decimal(10,8) DEFAULT '0.00000000',
  `to_wallet` varchar(250) DEFAULT NULL,
  `remarks` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `direct_referral`
--

CREATE TABLE `direct_referral` (
  `id` int(11) NOT NULL,
  `recruiter_id` int(11) DEFAULT '0',
  `recruitee_id` int(11) DEFAULT '0',
  `amount_earnings` decimal(10,2) DEFAULT '0.00',
  `is_withdrawed` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `dummy_accounts`
--

CREATE TABLE `dummy_accounts` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(250) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `invested_amount` decimal(10,2) DEFAULT '0.00',
  `withdrawed_amount` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `module_name` text,
  `content` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `module_name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'sell_value', '19', '2017-07-09 07:40:15', '2017-08-13 13:24:09'),
(2, 'buy_value', '24', '2017-07-09 07:40:15', '2017-08-13 13:24:23'),
(3, 'buy_value_btc', '0.0052', '2017-07-09 07:40:15', '2017-08-17 11:26:50'),
(4, 'sell_value_btc', '.0041', '2017-07-09 07:40:15', '2017-08-17 11:26:55'),
(5, 'package_1_range', '7', '2017-07-09 07:40:15', '2017-07-12 07:39:05'),
(6, 'package_1_interest', '1', '2017-07-09 07:40:15', NULL),
(7, 'package_1_lockin', '15', '2017-07-09 07:40:15', '2017-08-11 06:52:58'),
(8, 'package_2_range', '15', '2017-07-09 07:40:15', '2017-07-25 10:24:04'),
(9, 'package_2_interest', '3', '2017-07-09 07:40:15', '2017-08-11 06:47:43'),
(10, 'package_2_lockin', '30', '2017-07-09 07:40:15', '2017-08-11 06:42:00'),
(11, 'registration', '1', '2017-07-11 09:00:59', '2017-08-11 07:11:37'),
(12, 'payout', '1', '2017-07-11 09:01:07', '2017-08-11 07:11:37'),
(13, 'dr_bonus', '500', '2017-07-11 09:01:07', '2017-08-22 10:26:34');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrators', '{\"admin\":1}', '2016-09-07 06:12:06', '2016-09-07 06:12:06'),
(2, 'Member', '{\"member\":1}', '2016-09-07 06:12:07', '2016-09-07 06:12:07');

-- --------------------------------------------------------

--
-- Table structure for table `inquiries`
--

CREATE TABLE `inquiries` (
  `id` int(11) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `subject` varchar(500) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `message` text,
  `is_responded` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `interest_logs`
--

CREATE TABLE `interest_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `investment_code` varchar(250) DEFAULT NULL,
  `current_balance` decimal(10,8) DEFAULT '0.00000000',
  `interest_amount` decimal(10,8) DEFAULT '0.00000000',
  `interest_rate` decimal(10,2) DEFAULT '0.00',
  `is_withdrawed` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `investment_logs`
--

CREATE TABLE `investment_logs` (
  `id` int(11) NOT NULL,
  `trans_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `tc_amount` decimal(10,8) DEFAULT '0.00000000',
  `package_type` int(11) DEFAULT '0',
  `days_range` decimal(10,2) DEFAULT NULL,
  `interest` decimal(10,2) DEFAULT NULL,
  `lock_in` int(11) DEFAULT '0',
  `is_withdrawed` tinyint(1) DEFAULT '0',
  `is_due` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `investment_logs`
--

INSERT INTO `investment_logs` (`id`, `trans_code`, `user_id`, `tc_amount`, `package_type`, `days_range`, `interest`, `lock_in`, `is_withdrawed`, `is_due`, `created_at`, `updated_at`) VALUES
(1, 'Ljcskx3b', 8, 5.00000000, 1, 7.00, 1.00, 15, 0, 0, '2017-08-22 08:34:33', '2017-08-22 08:34:33');

-- --------------------------------------------------------

--
-- Table structure for table `master_password`
--

CREATE TABLE `master_password` (
  `id` int(11) NOT NULL,
  `password` varchar(255) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_password`
--

INSERT INTO `master_password` (`id`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'f97c959d3ad48cfb23cdb1a4f000db6a', 1, '2015-05-24 11:19:47', '2017-08-17 11:37:13'),
(2, '14080817575b9dc03fcd04eeaf42aada', 1, '2015-05-24 11:19:47', '2017-08-01 16:29:03');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(50) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `from_user_id` int(11) NOT NULL,
  `msg_body` longtext,
  `last_sender_user_id` int(11) DEFAULT '0',
  `is_read` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `ref_id`, `to_user_id`, `from_user_id`, `msg_body`, `last_sender_user_id`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 'g5dVePSUYu', 8, 1, NULL, 1, 1, '2017-08-22 10:50:18', '2017-08-22 10:56:07');

-- --------------------------------------------------------

--
-- Table structure for table `message_replies`
--

CREATE TABLE `message_replies` (
  `id` int(11) NOT NULL,
  `msg_ref_id` varchar(50) DEFAULT NULL,
  `msg_from_user_id` int(11) DEFAULT NULL,
  `reply_body` longtext,
  `is_read` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_replies`
--

INSERT INTO `message_replies` (`id`, `msg_ref_id`, `msg_from_user_id`, `reply_body`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 'g5dVePSUYu', 1, 'aaaaaa', 1, '2017-08-22 10:50:18', '2017-08-22 10:56:07');

-- --------------------------------------------------------

--
-- Table structure for table `misc_logs`
--

CREATE TABLE `misc_logs` (
  `id` int(11) NOT NULL,
  `trans_code` varchar(50) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT '0.00',
  `previous_balance` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payouts`
--

CREATE TABLE `payouts` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `request_type` tinyint(1) DEFAULT '0',
  `link_id` int(11) DEFAULT NULL,
  `tc_amount` decimal(10,8) DEFAULT '0.00000000',
  `status` tinyint(1) DEFAULT '0',
  `approved_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_logs`
--

CREATE TABLE `purchase_logs` (
  `id` int(11) NOT NULL,
  `hash` text,
  `address` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `btc_amount` decimal(10,8) DEFAULT NULL,
  `tc_amount` decimal(10,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_logs`
--

INSERT INTO `purchase_logs` (`id`, `hash`, `address`, `user_id`, `btc_amount`, `tc_amount`, `created_at`, `updated_at`) VALUES
(1, '8c32c7cff65351433f74bc0deb597cd6e469867de8a299863a6fc3b95d109cb7', '2N2S9u35Ni5sTbFnekkmZjNe5iBDftqHJmt', 8, 1.75718023, 99.99999999, '2017-08-22 15:57:32', '2017-08-22 15:57:32');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `attempts` varchar(45) DEFAULT NULL,
  `suspended` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT '',
  `middle_name` varchar(45) DEFAULT '',
  `last_name` varchar(255) DEFAULT '',
  `pin_code` int(4) DEFAULT '0',
  `associated_wallet` varchar(250) DEFAULT NULL,
  `avatar` varchar(500) DEFAULT 'default-avatar.png',
  `permissions` text,
  `user_type` tinyint(1) DEFAULT '3' COMMENT '1 = admin, 2 = cashier, 3 = member',
  `activated` tinyint(1) DEFAULT '1',
  `activation_code` varchar(255) DEFAULT '',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) DEFAULT NULL,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `is_registered` tinyint(1) DEFAULT '0',
  `is_password_changed` tinyint(1) DEFAULT '0',
  `has_investment` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_id`, `username`, `email`, `password`, `first_name`, `middle_name`, `last_name`, `pin_code`, `associated_wallet`, `avatar`, `permissions`, `user_type`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `is_registered`, `is_password_changed`, `has_investment`, `created_at`, `updated_at`) VALUES
(1, 'ZSMqmVNm', 'admin1', 'admin1@org.com', '$2y$10$JzTHhQgwaXVOdDkNMhlNRu9ObHiOF/ZI3Yw.PbTRfpzApzLYY/xWO', 'Admin 1', '', '', 0, NULL, 'default-avatar.png', '{\"admin\":1}', 1, 1, '', NULL, '2017-08-22 11:02:01', '$2y$10$/ZPsFBOaTW0MYT3I8TNKQ.D7lOBvWW.6aXgQR566CceEoWKBBpDRe', NULL, 0, 0, 0, '2016-09-07 06:12:06', '2017-08-22 11:02:01'),
(2, 'W9Wbb3ft', 'admin2', 'admin2@org.com', '$2y$10$9SGbBc5lOs1zbhIBDWOfPuk0dI/UwGt/T8205nszf6UhxAfZB.Owa', 'Admin 2', '', '', 0, NULL, 'default-avatar.png', '{\"admin\":1}', 1, 1, '', NULL, '2017-07-15 00:14:32', '$2y$10$TfC4UN3khBJZQdf4smNRt.lvy6AZGh3jb2WVimknrdI1zrY/OgtpC', NULL, 0, 0, 0, '2016-09-07 06:12:07', '2017-07-15 00:14:32'),
(3, '6hKmqNVc', 'admin3', 'admin3@org.com', '$2y$10$SSIoeG9vN8V.fLHrt0kE/O2Etd04sxthYyX9ARaaLc9zsa6gX2WyC', 'Admin 3', '', '', 0, NULL, 'default-avatar.png', '{\"admin\":1}', 1, 1, '', NULL, NULL, NULL, NULL, 0, 0, 0, '2016-09-07 06:12:07', '2016-09-07 06:12:07'),
(4, 'rmevMJx6', 'admin4', 'admin4@org.com', '$2y$10$nD3j55g4N50uZodcwucx2OchzSYPUjvukHqFm1WKlbXkQXWjTaFVa', 'Admin 4', '', '', 0, NULL, 'default-avatar.png', '{\"admin\":1}', 1, 1, '', NULL, NULL, NULL, NULL, 0, 0, 0, '2016-09-07 06:12:07', '2016-09-07 06:12:07'),
(5, '3cC5XCfZ', 'admin5', 'admin5@org.com', '$2y$10$SIRwz9zk1vlZ4REUy32r.u0I53K7O1rqD7XeGAtfx6FSsf7NgK3xG', 'Admin 5', '', '', 0, NULL, 'default-avatar.png', '{\"admin\":1}', 1, 1, '', NULL, NULL, NULL, NULL, 0, 0, 0, '2016-09-07 06:12:07', '2016-09-07 06:12:07'),
(6, 'e98aldgl', 'HEAD', 'HEAD@org.com', '$2y$10$4cQn7qtrzSNB3NQk61eAUe016q47o/fR/RQjoaFu/jCxnlviHEuwa', 'Company', '', 'Head', 0, NULL, 'default-avatar.png', '{\"member\":1}', 3, 1, '', NULL, '2016-09-08 16:07:57', '$2y$10$loEFCg.pyv8rNm78j2HF3uYNqVVL9MmYXu/QAT6jifjRM8Vr87tGm', NULL, 0, 0, 0, '2016-09-07 06:12:07', '2016-09-22 11:43:38'),
(7, 'TrdzYLF7', 'acsolution', 'acsolution@org.com', '$2y$10$agRZonlQMwiucHyyonPzT.MCnbolJf9Vq8FbGk0RshOtT.kWZVDV.', 'ACS', '', 'IT', 0, NULL, 'default-avatar.png', '{\"admin\":1}', 1, 1, '', NULL, '2016-09-27 15:48:16', '$2y$10$mSPxuq6Lu8WcnAOVQxKMLeRkbdPdjp3jHfvS0eFI5KHsMSNEvptsi', NULL, 0, 0, 0, '2016-09-07 06:12:07', '2016-09-27 15:48:16'),
(8, 'KcB423A3fEew', 'ACE83726018', 'ullen.d.faustino@gmail.com', '$2y$10$MtwyZQX03WCYgv6JYiug8OFVsu3IWc3d/nXs8FQp6y7xK5/y70DGq', 'Ullen Faustino', '', '', 1234, '2N2S9u35Ni5sTbFnekkmZjNe5iBDftqHJmt', 'Philippines.png', '{\"member\":1}', 3, 1, NULL, '2017-08-22 15:51:18', '2017-08-22 11:05:44', '$2y$10$X7UU/NAJk6mPlIGS5hqw4eUSsvG1Z3dpiNQea148jthh/SFJ3pQSe', NULL, 1, 1, 1, '2017-08-22 15:50:54', '2017-08-22 11:37:29');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 2),
(7, 2),
(8, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_withdrawals`
--
ALTER TABLE `admin_withdrawals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `change_logs`
--
ALTER TABLE `change_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_allocated_income`
--
ALTER TABLE `company_allocated_income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `convertion_logs`
--
ALTER TABLE `convertion_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `direct_referral`
--
ALTER TABLE `direct_referral`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dummy_accounts`
--
ALTER TABLE `dummy_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiries`
--
ALTER TABLE `inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interest_logs`
--
ALTER TABLE `interest_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_logs`
--
ALTER TABLE `investment_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_password`
--
ALTER TABLE `master_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_replies`
--
ALTER TABLE `message_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `misc_logs`
--
ALTER TABLE `misc_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payouts`
--
ALTER TABLE `payouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_logs`
--
ALTER TABLE `purchase_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_throttle_users_idx` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_id` (`ref_id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `fk_users_group_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_withdrawals`
--
ALTER TABLE `admin_withdrawals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `change_logs`
--
ALTER TABLE `change_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `company_allocated_income`
--
ALTER TABLE `company_allocated_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `convertion_logs`
--
ALTER TABLE `convertion_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `direct_referral`
--
ALTER TABLE `direct_referral`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dummy_accounts`
--
ALTER TABLE `dummy_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `inquiries`
--
ALTER TABLE `inquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `interest_logs`
--
ALTER TABLE `interest_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `investment_logs`
--
ALTER TABLE `investment_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `master_password`
--
ALTER TABLE `master_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `message_replies`
--
ALTER TABLE `message_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `misc_logs`
--
ALTER TABLE `misc_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payouts`
--
ALTER TABLE `payouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_logs`
--
ALTER TABLE `purchase_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
